<?php
require_once "../Core/autoload.php";
require_once '../Core/Debugger/dumper.php';

/*
 * Définition des routes
 * Exemple :
 * Route normale :
 * App\Router::url('/ma/route', 'controleur, 'methode');
 *
 * Route avec paramètres
 * App\Router::url('/ma/route/?param1/?param2, 'controleur', 'methode');
 *  => Dans la vrai URL (navigateur) : Ne pas mettre '?' pour représenter les paramètres
 *  => Paramètres à traiter / valider dans le controleur
 */
$core = \Core\Core::init();


