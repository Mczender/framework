<?php
namespace App\Form;

use Core\Form\AbstractForm;
use Core\Form\Builder\FormBuilder;
use Core\Form\Fields\TextField;
use Core\Form\Resolvers\OptionsResolver;
use Src\Tables\Contact;

class ContactForm extends AbstractForm
{
    public function buildForm(FormBuilder $formBuilder, array $options = []): void
    {
        $formBuilder->add('name', TextField::class, ['required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault([
            'data_class' => Contact::class,
            'test' => null
        ]);
    }
}