<?php

namespace Src\Repository;

use Src\Tables\User;

class UserRepository extends \Core\Repository\AbstractRepository
{
    private $userTable;

    public function __construct()
    {
        parent::__construct(User::class);
    }
}
                