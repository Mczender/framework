<?php

namespace Src\Repository;

use Src\Tables\Software;

class SoftwareRepository extends \Core\Repository\AbstractRepository
{
    private $softwareTable;

    public function __construct()
    {
        parent::__construct(Software::class);
    }
}
                