<?php

namespace Src\Repository;

use Src\Tables\Doctrine_migration_versions;

class Doctrine_migration_versionsRepository extends \Core\Repository\AbstractRepository
{
    private $doctrine_migration_versionsTable;

    public function __construct()
    {
        parent::__construct(Doctrine_migration_versions::class);
    }
}
                