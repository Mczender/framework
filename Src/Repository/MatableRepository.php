<?php

namespace Src\Repository;

use Src\Tables\Matable;

class MatableRepository extends \Core\Repository\AbstractRepository
{
    private $matableTable;

    public function __construct()
    {
        parent::__construct(Matable::class);
    }
}
                