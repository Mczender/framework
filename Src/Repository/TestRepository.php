<?php

namespace Src\Repository;

use Src\Tables\Test;

class TestRepository extends \Core\Repository\AbstractRepository
{
    private $testTable;

    public function __construct()
    {
        parent::__construct(Test::class);
    }
}
                