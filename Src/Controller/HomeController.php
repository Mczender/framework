<?php

namespace App\Controller;

use App\Form\ContactForm;
use Core\Exceptions\Controller\TemplateNotFindException;
use Core\Controller\AbstractController;
use Core\Routing\Routes\Attributs\Route;

//Doit extends de AbstractController
class HomeController extends AbstractController
{
    /**
     * @return void
     * @throws TemplateNotFindException
     */
    #[Route(name: 'home', path: '/')]
    public function index(): void
    {
        $this->render('index');
    }
}