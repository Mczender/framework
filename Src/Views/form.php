<?php $title = "Générateur de groupe" ?>
<form action="/groupes" method="post" class="form-container" enctype="multipart/form-data">

    <div class="content-form-row">
        <input name="file-uploader" type="file" class="input-file" required>
        <a href="/aide">Besoin d'aide ?</a>
    </div>

    <div class="content-form-row">
        <label for="number">Personnes par groupes</label>
        <input type="number" name="number" class="input-number" required>
    </div>

    <input type="submit" value="Valider" class="input-submit">
</form>