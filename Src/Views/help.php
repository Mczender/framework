<h2>Aide</h2>
<p class="p-help">Comment formater votre fichier excel ?</p>
<table>
    <thead>
        <tr>
            <th>Civilité</th>
            <th>Nom</th>
            <th>Prénom</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>M.</td>
            <td>Doe</td>
            <td>John</td>
        </tr>
        <tr>
            <td>M.</td>
            <td>Golo</td>
            <td>Thierry</td>
        </tr>
        <tr>
            <td>Mme.</td>
            <td>Lea</td>
            <td>Smith</td>
        </tr>
        <tr>
            <td>...</td>
            <td>...</td>
            <td>...</td>
        </tr>
    </tbody>
</table>

<p>Votre tableau excel doit respecter exactement cette structure. Les termes "Civilité", "Nom" et "Prénom" doivent être présents</p>
<a href="/" class="link-form">Retour au formulaire</a>