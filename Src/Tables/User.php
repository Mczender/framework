<?php
namespace Src\Tables;

use Core\Table\AbstractTable;
use Core\ORM\Collection;
use DateTime;

class User extends AbstractTable
{

    private String $email;
                    
    private String $roles;
                    
    private String $password;
                    
    private String $username;
                    
    public function getEmail() : String
    {
        return $this->email;
    }
    
    public function setEmail(String $email)
    {
        $this->email = $email;
        
        return $this;
    }
                    
    public function getRoles() : String
    {
        return $this->roles;
    }
    
    public function setRoles(String $roles)
    {
        $this->roles = $roles;
        
        return $this;
    }
                    
    public function getPassword() : String
    {
        return $this->password;
    }
    
    public function setPassword(String $password)
    {
        $this->password = $password;
        
        return $this;
    }
                    
    public function getUsername() : String
    {
        return $this->username;
    }
    
    public function setUsername(String $username)
    {
        $this->username = $username;
        
        return $this;
    }
                    
}