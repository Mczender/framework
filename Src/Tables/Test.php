<?php
namespace Src\Tables;

use Core\Table\AbstractTable;
use Core\ORM\Collection;
use DateTime;

class Test extends AbstractTable
{

    private String $titre;
                    
    private Collection $software;

                    
    public function __construct()
    {
        $this->software = new Collection('Src\Repository\SoftwareRepository');
    }

    public function getTitre() : String
    {
        return $this->titre;
    }
    
    public function setTitre(String $titre)
    {
        $this->titre = $titre;
        
        return $this;
    }
                    
    public function getSoftware() : Collection
    {
        return $this->software;
    }
    
    public function setSoftware(?Software $software)
    {
        $this->software->add($software);
        
        return $this;
    }
                    
}