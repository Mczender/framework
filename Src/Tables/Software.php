<?php
namespace Src\Tables;

use Core\Table\AbstractTable;
use Core\ORM\Collection;
use Src\Repository\MatableRepository;
use DateTime;

class Software extends AbstractTable
{

    private String $name;
                    
    private String $link_site;
                    
    private String $lastversion;
                    
    private String $slug;
                    
    private ?String $download_zip;
                    
    private ?String $download_tar;
                    
    private $test;

    private Collection $matable;

                    
    public function __construct()
    {
        $this->matable = new Collection('Src\Repository\MatableRepository');
    }

    public function getName() : String
    {
        return $this->name;
    }
    
    public function setName(String $name)
    {
        $this->name = $name;
        
        return $this;
    }
                    
    public function getLink_site() : String
    {
        return $this->link_site;
    }
    
    public function setLink_site(String $link_site)
    {
        $this->link_site = $link_site;
        
        return $this;
    }
                    
    public function getLastversion() : String
    {
        return $this->lastversion;
    }
    
    public function setLastversion(String $lastversion)
    {
        $this->lastversion = $lastversion;
        
        return $this;
    }
                    
    public function getSlug() : String
    {
        return $this->slug;
    }
    
    public function setSlug(String $slug)
    {
        $this->slug = $slug;
        
        return $this;
    }
                    
    public function getDownload_zip() : String
    {
        return $this->download_zip;
    }
    
    public function setDownload_zip(?String $download_zip)
    {
        $this->download_zip = $download_zip;
        
        return $this;
    }
                    
    public function getDownload_tar() : String
    {
        return $this->download_tar;
    }
    
    public function setDownload_tar(?String $download_tar)
    {
        $this->download_tar = $download_tar;
        
        return $this;
    }
                    
    public function getTest() : Test
    {
        return $this->test;
    }
    
    public function setTest(?Test $test)
    {
        $this->test = $test;
        
        return $this;
    }
                    
    public function getMatable() : Collection
    {
        return $this->matable;
    }
    
    public function setMatable(Matable $matable)
    {
        $this->matable->add($matable);
        
        return $this;
    }
                    
}