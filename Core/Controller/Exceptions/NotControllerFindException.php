<?php

namespace Core\Controller\Exceptions;

use Core\Exceptions\FrameworkException;

class NotControllerFindException extends FrameworkException
{

}