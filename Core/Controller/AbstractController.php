<?php

namespace Core\Controller;

use Core\Exceptions\Controller\TemplateNotFindException;
use Core\Form\Factory\FormFactory;
use Core\Form\Form;
use Core\Table\AbstractTable;
use JetBrains\PhpStorm\NoReturn;

abstract class AbstractController
{
    protected ?string $title = null;

    /**
     * Permet de rendre une vue
     * @throws TemplateNotFindException
     */
    protected function render(String $template, array $options = []) : void
    {
        extract($options);

        if (file_exists('../Src/Views/' . $template . '.php'))
            require '../Src/Views/' . $template . '.php';
        else
            throw new TemplateNotFindException("Le template \"../Src/Views/" . $template . ".php\" n'a pas été trouvée. Vérifiez le template demandé ou ajoutez le", 500);
    }

    /*
     * Permet de faire une redirection au sein d'un controleur
     */
    #[NoReturn]
    protected function redirect(String $route)
    {
        header('Location: '. $route);
        exit;
    }

    final protected function createForm(string $form_class, ?AbstractTable $data = null, array $options = []): Form
    {
        $formFactory = new FormFactory();
        $formFactory->createForm($form_class, $data, $options);
    }

    /*
     * Permet d'obtenir le titre d'une page
     * → Le titre doit être défini dans une méthode d'un contrôleur concret comme ceci:
     * $this->title = "Mon titre";
     */
    public function getPageTitle()
    {
        return $this->title;
    }
}