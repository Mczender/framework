<?php

namespace Core\Config\Exceptions;

use Core\Exceptions\FrameworkException;

class NotBddConfigException extends FrameworkException
{

}