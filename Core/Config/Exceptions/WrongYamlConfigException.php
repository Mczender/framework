<?php

namespace Core\Config\Exceptions;

class WrongYamlConfigException extends \Core\Exceptions\FrameworkException
{
}