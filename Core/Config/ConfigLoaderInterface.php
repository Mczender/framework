<?php

namespace Core\Config;

interface ConfigLoaderInterface
{
    public function loadConfig(): array;
}