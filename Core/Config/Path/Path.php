<?php
namespace Core\Config\Path;

use Core\Config\ConfigLoaderFactory;
use Core\Config\ConfigLoaderInterface;
use Core\Config\Exceptions\NotFileConfigException;
use Core\Config\Exceptions\WrongYamlConfigException;

/**
 * Retourne tous les chemins et namespace des différents composants du framework
 */
final class Path
{
    private static array $paths = [
        'controller' => 'Src/Controller',
        'command'    => 'Src/Command',
        'table'      => 'Src/Table',
    ];

    private static array $name_spaces = [
        'controller' => 'App\\Controller',
        'command'    => 'App\\Command',
        'table'      => 'App\\Table',
    ];

    /**
     * Retourne le chemin absolu racine du projet
     * @return string
     */
    public static function getRootProjectDir(): string
    {
        return dirname(__DIR__, 3) . DIRECTORY_SEPARATOR;
    }

    public static function getCoreDirectory(): string
    {
        return dirname(__DIR__, 2);
    }
    
    /**
     * Retourne le chemin absolu du fichier /config du projet
     * @return string
     */
    public static function getConfigDirectory(): string
    {
        return self::getRootProjectDir() . 'config' . DIRECTORY_SEPARATOR;
    }

    /**
     * Retourne le chemin absolu du fichier de routage yaml (/config/routes.yaml)
     * @return string
     */
    public static function getRoutesYamlFile(): string
    {
        return self::getConfigDirectory() . 'routes.yaml';
    }

    /**
     * Retourne le répertoire où sont stockés les contrôleurs du code source du projet (chemin défini dans /config/config.yaml)
     *
     * @return string|null
     */
    public static function getControllerDirectory(): ?string
    {
        return convert_directory_separator_in_path(self::$paths['controller']);
    }

    public static function getControllerNameSpace(): ?string
    {
        return self::$name_spaces['controller'];
    }

    public static function getTableDirectory(): ?string
    {
        return convert_directory_separator_in_path(self::$paths['table']);
    }

    public static function getTableNamespace(): ?string
    {
        return self::$name_spaces['table'];
    }

    public static function getCommandDirectory(): ?string
    {
        return convert_directory_separator_in_path(self::$paths['command']);
    }

    public static function getCommandNamespace(): ?string
    {
        return self::$name_spaces['command'];
    }

    public static function getAllNamespaces(): array
    {
        return self::$name_spaces;
    }

    public static function getAllPaths(): array
    {
        return self::$paths;
    }

    /**
     * Charge tous les chemins sources du projet définis dans /config/config.yaml
     * Permet de gérer la surcharge des path depuis le fichier config.yaml
     *
     * @throws WrongYamlConfigException
     */
    public static function loadConfig(ConfigLoaderInterface $config_loader): void
    {
        $path_loader = new PathConfigLoader($config_loader);
        $namespace_loader = new NamespaceConfigLoader($config_loader);

        foreach (array_keys(self::$paths) as $module) {
            if (null !== $custom_path = $path_loader->setPathConfig($module)) {
                self::$paths[$module] = $custom_path;
            } else {
                self::$paths[$module] = self::getRootProjectDir() . self::$paths[$module];
            }
        }

        foreach (array_keys(self::$name_spaces) as $module) {
            if (null !== $custom_namespace = $namespace_loader->setNamespaceConfig($module)) {
                self::$name_spaces[$module] = $custom_namespace;
            }
        }
    }
}