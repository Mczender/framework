<?php

namespace Core\Config\Path;

use Core\Config\ConfigLoaderInterface;

readonly class NamespaceConfigLoader
{
    public function __construct(private ConfigLoaderInterface $configLoader)
    {
    }

    public function setNamespaceConfig(string $property): ?string
    {
        $path_config = $this->configLoader->loadConfig()['path'] ?? null;

        if ($path_config === null) {
            return null;
        }

        return $path_config[$property]['namespace'] ?? null;
    }
}