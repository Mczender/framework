<?php

namespace Core\Config\Path;

use Core\Config\ConfigLoaderInterface;
use Core\Config\Exceptions\WrongYamlConfigException;

readonly class PathConfigLoader
{
    public function __construct(private ConfigLoaderInterface $configLoader)
    {
    }

    /***
     * @param string $property
     * @return string|null
     * @throws WrongYamlConfigException
     */
    public function setPathConfig(string $property): ?string
    {
        $path_config = $this->configLoader->loadConfig()['path'] ?? null;

        if ($path_config === null) {
            return null;
        }

        $property_path = (Path::getRootProjectDir() . $path_config[$property]['path']) ?? null;

        if ($property_path !== null && !is_dir($property_path)) {
            throw new WrongYamlConfigException("Le path des \"$property\" a mal été configuré. Le répertoire {$property_path} n'existe pas.", 500);
        }

        return $property_path;
    }
}