<?php

namespace Core\Config;

use Core\Config\Exceptions\NotFileConfigException;
use Core\Config\Path\Path;

class ConfigLoaderFactory 
{
    private const array SUPPORTED_FILE_CONFIG_EXTENSION = [
        'yaml', 'json', 'php', 'xml'
    ];

    public static function create(): ConfigLoaderInterface
    {
        $configDir = Path::getConfigDirectory();

        foreach (self::SUPPORTED_FILE_CONFIG_EXTENSION as $extension) {
            $file = $configDir . 'config.' . $extension;
            if (file_exists($file)) {
                return match ($extension) {
                    'yaml' => new YamlConfigLoader(),
                    default => throw new NotFileConfigException("Type de fichier de configuration $extension non pris en charge"),
                };
            }
        }
    }
}