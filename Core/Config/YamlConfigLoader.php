<?php
namespace Core\Config;

use Core\Config\Exceptions\NotFileConfigException;
use Core\Config\Path\Path;
use Core\Parser\YAML;

readonly class YamlConfigLoader implements ConfigLoaderInterface
{
    private YAML $config;

    /**
     * @throws NotFileConfigException
     */
    public function __construct()
    {
        if (!file_exists($config_file = Path::getConfigDirectory() . 'config.yaml')) {
            throw new NotFileConfigException('Le fichier de configuration "/config/config.yaml" n\'existe pas !', 500);
        }

        $this->config = new YAML($config_file);
    }

    public function loadConfig(): array
    {
        return $this->config->get('framework');
    }
}