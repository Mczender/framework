<?php

namespace Core\Config\bdd;

use Core\Config\ConfigLoaderInterface;
use Core\Config\Exceptions\NotBddConfigException;
use Core\Config\Exceptions\WrongYamlConfigException;

readonly class ConnexionConfigLoader
{
    public const string CONFIG_DB_TYPE = 'type';
    public const string CONFIG_DB_VERSION = 'version';
    public const string CONFIG_DB_HOST = 'host';
    public const string CONFIG_DB_NAME = 'name';
    public const string CONFIG_DB_USER = 'user';
    public const string CONFIG_DB_PASSWORD = 'password';

    private ?array $bdd_config;

    public function __construct(private ConfigLoaderInterface $config_loader)
    {
        $this->bdd_config = $this->config_loader->loadConfig()['bdd'] ?? null;
    }

    /**
     * TODO : Revoir le système de configuration de la base de données pour l'améliorer et le simplifier coté configuration YAML
     *
     * @param string $property
     * @param string|null $connexion
     * @return string
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public function getConnexionConfig(string $property, ?string $connexion = null): string
    {
        if ($this->bdd_config === null) {
            throw new NotBddConfigException("La configuration de la base de données n'est pas définie.", 500);
        }

        $bdd_connexion = $bdd_config[$connexion] ?? reset($bdd_config) ?? null;

        if ($bdd_connexion === null) {
            if ($connexion !== null) {
                throw new NotBddConfigException("La connexion $connexion n'existe pas.", 500);
            }

            throw new NotBddConfigException("Aucune connexion à une base de données n'a été définie", 500);
        }

        $available_config = [self::CONFIG_DB_TYPE, self::CONFIG_DB_VERSION, self::CONFIG_DB_HOST, self::CONFIG_DB_NAME, self::CONFIG_DB_USER, self::CONFIG_DB_PASSWORD];

        if (!in_array($property, $available_config)) {
            throw new WrongYamlConfigException("La propriété $property est invalide dans la configuration de la base de données.", 500);
        }

        if (!array_key_exists($property, $bdd_config)) {
            throw new WrongYamlConfigException("La propriété $property n'est pas définie dans la configuration de la base de données.", 500);
        }

        return $bdd_config[$property];
    }
}