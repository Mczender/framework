<?php

namespace Core\Config\bdd;

use Core\Config\ConfigLoaderInterface;
use Core\Config\Exceptions\NotBddConfigException;
use Core\Config\Exceptions\WrongYamlConfigException;

class Connexion
{
    private static ConnexionConfigLoader $connexionConfigLoader;

    /**
     * @param string|null $connexion
     * @return string
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public static function getType(?string $connexion = null): string
    {
        return self::$connexionConfigLoader->getConnexionConfig(ConnexionConfigLoader::CONFIG_DB_TYPE, $connexion);
    }

    /**
     * @param string|null $connexion
     * @return string
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public static function getVersion(?string $connexion = null): string
    {
        return self::$connexionConfigLoader->getConnexionConfig(ConnexionConfigLoader::CONFIG_DB_VERSION, $connexion);
    }

    /**
     * @param string|null $connexion
     * @return string
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public static function getHost(?string $connexion = null): string
    {
        return self::$connexionConfigLoader->getConnexionConfig(ConnexionConfigLoader::CONFIG_DB_HOST, $connexion);
    }

    /**
     * @param string|null $connexion
     * @return string
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public static function getDatabase(?string $connexion = null): string
    {
        return self::$connexionConfigLoader->getConnexionConfig(ConnexionConfigLoader::CONFIG_DB_NAME, $connexion);
    }

    /**
     * @param string|null $connexion
     * @return string
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public static function getUser(?string $connexion = null): string
    {
        return self::$connexionConfigLoader->getConnexionConfig(ConnexionConfigLoader::CONFIG_DB_USER, $connexion);
    }

    /**
     * @param string|null $connexion
     * @return string
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public static function getPassword(?string $connexion = null): string
    {
        return self::$connexionConfigLoader->getConnexionConfig(ConnexionConfigLoader::CONFIG_DB_PASSWORD, $connexion);
    }

    /**
     * @param ConfigLoaderInterface $configLoader
     * @return void
     */
    public static function loadConfig(ConfigLoaderInterface $configLoader): void
    {
        self::$connexionConfigLoader = new ConnexionConfigLoader($configLoader);
    }
}