<?php

namespace Core\Command\Command;

use Core\Command\AbstractCMD;
use Core\Command\Attributes\Command;
use Core\Config\Path\Path;

#[Command('create:table')]
class CreateTableCMD extends AbstractCMD
{
    private const array AVAILABLE_TYPES_FIELD_TABLE = [
        'string    -> Permet de stocker du texte jusqu\'a 255 charactères'  => 'string',
        'text      -> Permet de stocker de longs texte'                     => 'text',
        'integer   -> Permet de stocker des nombres entiers'                => 'integer',
        'real      -> Permet de stocker des réel à virgule flottante'       => 'real',
        'boolean   -> Permet de stocker deux valeues "TRUE" ou "FALSE"'     => 'boolean',
        'datetime  -> Permet de stocker des dates au format DateTime'       => 'datetime',
        'timestamp -> Permet de stocker des timestamp'                      => 'timestamp',
        'relation  -> Permet de créer une relation avec une autre table (OneToOne, ManyToOne, OneToMany, ManyToMany)' => 'relation',
        '?' => '?',
    ];

    public function __construct()
    {
        $this->help = 'Commande permettant de générer une nouvelle table. Pour fonctionner, la connexion à la base de donnée doit être configurée. Une classe Table et une table en BD sont générées';
    }

    public function run()
    {
        $tableName = $this->ask('Le nom de votre table');

        $fields = [];
        if ($tableName !== null) {
            $this->prompt('Commencez à ajouter des champs !');
            $addFields = true;
            while($addFields) {
                $fieldName = $this->ask("Le nom de votre nouveau champ");
                $fieldName === null ? $addFields = false : $addFields = true;

                if ($addFields === true) {
                    $fieldType = $this->getTypeField();
                    while($fieldType === '?') {
                        $fieldType = $this->getTypeField(true);
                    } //while

                    $size = 0;
                    switch ($fieldType) {
                        case 'string':
                            $size = $this->ask("Combien de charactères peut t'il y avoir maximum (entre 1 et 255) [255]", range(1, 255), 255, true, false);
                            break;
                        case 'relation':
                            $targetTable = null;
                            do {
                                $targetTable = $this->ask(text: 'Avec quel table existante souhaitez vous créer une relation', allow_null_choice: false);
                            } while(!class_exists(Path::getTableNamespace() . '\\' . $targetTable));

                            $relationType = $this->getRelationTypeField($tableName, $targetTable);
                            break;
                    } //switch

                    $isNull = $this->ask("Ce champ peut-il être null en base (Yes/Oui, No/Non) [No]", ['yes' => true, 'oui' => true, 'no' => false, 'non' => false], false, true, false);
                    $isUnique = $this->ask('Ce champ doit-il être unique en base (Yes/Oui No/Non) [No]', ['yes' => true, 'oui' => true, 'no' => false, 'non' => false], false, true, false);

                    $fields[] = ['name' => $fieldName, 'type' => $fieldType, 'null' => $isNull, 'unique' => $isUnique, 'size' => $size ?? null, 'targetTable' => $targetTable ?? null, 'relationType' => $relationType ?? null];
                } //if
            } //while
            //On crée la Table et le Repo que si on a définit des champs

        } //if
    }

    private function getTypeField(bool $show_choices = false): string
    {
        return $this->ask(
            text: "Quel doit être le type de votre champ ? ([?] pour voir la liste) [string]",
            choices: self::AVAILABLES_TYPES_FIELD_TABLE,
            default_value: 'string',
            show_choices: $show_choices
        );
    }

    private function getRelationTypeField(string $tableName, string $targetTable): string
    {
        $choices = [
            "OneToOne    : $tableName peut avoir qu'un $targetTable et $targetTable peut avoir qu'un $tableName"        => 'OneToOne',
            "OneToMany   : $tableName peut avoir qu'un $targetTable et $targetTable peut avoir plusieur $tableName"     => 'OneToMany',
            "ManyToOne   : $tableName peut avoir plusieur $targetTable et $targetTable peut avoir qu'un $tableName"     => 'ManyToOne',
            "ManyToMany : $tableName peut avoir plusieur $targetTable et $targetTable peut avoir plusieur $tableName"   => 'ManyToMany'
        ];

        return $this->ask("Quel doit être le type de la relation [ManyToOne] ?", $choices, 'ManyToOne');
    }
}