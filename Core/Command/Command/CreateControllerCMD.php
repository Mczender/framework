<?php

namespace Core\Command\Command;

use Core\Command\AbstractCMD;

class CreateControllerCMD extends AbstractCMD
{
    private $controllerName;

    public function __construct()
    {
        $this->cmdName = "create:controller";
        //Définit une aide qui s'affiche lorsque l'on tape -help en argument
        $this->help = 'Commande permettant de générer un controller basique et une par défaut';
    }
    
    public function run()
    {
        $this->controllerName = readline("Le nom de votre contrôleur ('Entrer' pour annuler) > ");

        if (!is_null($this->controllerName) && !empty($this->controllerName)) {
            echo 'true';
            file_put_contents('Src/Controller/' . ucfirst($this->controllerName) . 'Controller.php',
                '<?php

namespace Src\Controller;
use Core\Controller\AbstractController;

class ' . ucfirst($this->controllerName) . 'Controller extends AbstractController
{
    public function index()
    {
        $this->title = "' . ucfirst($this->controllerName) . 'Controller";
        $this->render("' . $this->controllerName . '/index");
    }   
}
        '
            );
            mkdir('Src/Views/' . $this->controllerName . '/');
            file_put_contents('Src/Views/' . $this->controllerName . '/index.php', '
<h1>' . ucfirst($this->controllerName) . 'Controller</h1>
        ');
        } else {
            echo 'Aucun nom saisi, opération annulée !';
        }
    }
}