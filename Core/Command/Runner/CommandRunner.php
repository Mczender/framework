<?php

namespace Core\Command\Runner;

use Core\Command\Attributes\Command;
use Core\Config\Path\Path;
use ReflectionException;

class CommandRunner
{
    private array $commands = [];

    /**
     * @throws ReflectionException
     */
    public function __construct()
    {
        $this->registerCommands();
    }

    public function runCommand(string $command): void
    {
        if (null === $command = $this->commands[$command] ?? null) {
            //TODO : Créer une classe Prompter / Console permettant de gérer l'affichage propre dans le terminal (utiliser les méthodes créées dans AbstractCMD
            echo "La commande $command n'existe pas !";
            return;
        }
        //TODO Injecter ici la classe Prompter et modifier la signature de la méthode run dans AbstractCMD

        $command->run();
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    private function registerCommands(): void
    {
        $core_commands_dir = Path::getCoreDirectory() . DIRECTORY_SEPARATOR . 'Command' . DIRECTORY_SEPARATOR . 'Command';
        $app_commands_dir  = Path::getCommandDirectory();

        foreach ([$core_commands_dir => 'Core\\Command\\Command', $app_commands_dir => Path::getCommandNamespace()] as $dir => $namespace) {
            $this->registerCommandsDir($dir, $namespace);
        }
    }

    /**
     * @param string $dir
     * @param string $namespace
     * @return void
     * @throws ReflectionException
     */
    private function registerCommandsDir(string $dir, string $namespace): void
    {
        if ($dir !== Path::getCommandDirectory()) {
            $files = new \RecursiveDirectoryIterator($dir);
        } else {
            if (is_dir($dir)) {
                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir));
            }
        }

        foreach ($files ?? [] as $file) {
            if ($file->isDir()) continue;
            $explodedClass = explode('\\', $file->getRealPath());
            $className = end($explodedClass);
            $className = substr($className, 0, -4);
            if (class_exists($namespace . '\\' . $className)) {
                $reflector = new \ReflectionClass($namespace . '\\' . $className);
                $attributes = $reflector->getAttributes(Command::class);
                foreach ($attributes as $attribute) {
                    $this->commands[$attribute->newInstance()->name] = $reflector->newInstance();
                }
            }
        }
    }
}