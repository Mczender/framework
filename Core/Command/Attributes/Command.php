<?php

namespace Core\Command\Attributes;

use Attribute;

#[Attribute]
class Command
{
    public function __construct(public string $name)
    {
        
    }
}