<?php

namespace Core\Command;

abstract class AbstractCMD
{
    protected string $help = '';

    public function getHelp(): string
    {
        echo $this->help;
    }

    abstract public function run();

    protected function prompt(string $text): void
    {
        echo "\033[0;32m" . $text . PHP_EOL . "\033[0m";
    }

    protected function error(string $text): void
    {
        echo "\033[0;37;41m" . $text . "\033[0m";
    }

    protected function ask(string $text, array $choices = [], null|int|float|string $default_value = null, bool $allow_null_choice = true, bool $show_choices = true): mixed
    {
        $question = $text;
        if (!empty($choices)) {
            if (!is_assoc($choices)) {
                $choices = array_combine($choices, $choices);
            }

            if ($show_choices) {
                foreach ($choices as $label => $choice) {
                    $question .= PHP_EOL . ' - ' . $label;
                }

                $question .= PHP_EOL;
            }
        }

        $this->prompt($question . ' > ');

        $answer = trim(fgets(STDIN));

        if (!empty($choices)) {
            $nbPossibleMatches = 0;
            foreach ($choices as $label => $choice) {
                if (str_starts_with($label, $answer)) {
                    $nbPossibleMatches++;
                    $answer = $choice;
                }
            }

            if ($nbPossibleMatches > 1 || (!$allow_null_choice && empty($answer))) {
                $this->error('Merci de sélectionner un choix valide !');
                return $this->ask($question, $choices);
            } elseif ($allow_null_choice && empty($answer)) {
                $answer = $default_value;
            }
        }

        return !empty($answer) ? $answer : null;
    }
}