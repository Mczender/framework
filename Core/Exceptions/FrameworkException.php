<?php
namespace Core\Exceptions;

class FrameworkException extends \Exception
{
    public function renderErrorPage(string $exceptionType)
    {
        $code = $this->getCode();
        $line = $this->getLine();
        $message = $this->getMessage();
        $file = $this->getFile();
        $trace = $this->getTrace();
        $preview = $this->getPrevious();
        require 'Template/Exception.php';
    }
}