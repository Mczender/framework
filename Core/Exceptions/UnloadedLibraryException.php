<?php

namespace Core\Exceptions;

use Core\Exceptions\FrameworkException;

class UnloadedLibraryException extends FrameworkException
{

}