<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/style/Exception/Style.css">
    <link rel="stylesheet" href="/assets/style/debug/debug.css">

    <title>Document</title>
</head>
<body>
    <header class="header-page">
        <h1>Une erreur s'est produite !</h1>
    </header>
    <main>
        <h2>Erreur <?= $code ?></h2>

        <div class="error-container">
            <header class="error-header">
                <h3><?= $exceptionType ?> :</h3>
                <p><?= $message ?></p>
            </header>
            <div class="main-error">
                <p class="file-error">Erreur survenue dans le fichier <strong>"<?= $file ?>"</strong> à la ligne <strong><?= $line ?></strong></p>
                <section class="stack-trace-container">
                    <h3>stack trace : </h3>
                    <ul>
                        <?php foreach ($trace as $call) : ?>
                            <li>
                                <div <?= str_starts_with($call['class'], 'Src\\') ? 'class="call"' : '' ?>>
                                    <strong>
                                        <?= $call['class'] . $call['type'] . $call['function'] ?>
                                    </strong>
                                    <p><?= $call['file'] ?> à la ligne <?= $call['line'] ?></p>
                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </section>
            </div>
        </div>
    </main>
</body>
</html>
