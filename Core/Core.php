<?php
namespace Core;

use Core\Config\bdd\Connexion;
use Core\Config\ConfigLoaderFactory;
use Core\Config\Exceptions\NotFileConfigException;
use Core\Config\Path\Path;
use Core\Exceptions\FrameworkException;
use Core\Routing\Router;
use JetBrains\PhpStorm\NoReturn;

final class Core
{
    private static ?self $instance = null;

    /**
     * Utiliser Core::init pour obtenir l'instance unique de la classe
     */
    #[NoReturn]
    private function __construct()
    {
        $router = new Router();

        try {
            $this->loadConfig();
            $router->match();
        } catch (FrameworkException $e) {
            $e->renderErrorPage(get_class($e));
        }
    }

    /**
     * Permet d'obtenir une instance unique de la classe Core
     * @return Core
     */
    public static function init(): Core
    {
        if (!is_null(self::$instance)) {
            return self::$instance;
        }

        return self::$instance = new self;
    }
    
    /**
     * @throws Routing\Routes\Exceptions\RouteFormatExcpetion
     * @throws Routing\Routes\Exceptions\RouteConfigException
     * @throws Controller\Exceptions\NotMethodFindException
     * @throws Routing\Parser\Exceptions\RouteYAMLFileException
     * @throws Controller\Exceptions\NotControllerFindException
     * @throws NotFileConfigException
     * @throws Config\Exceptions\WrongYamlConfigException
     */
    private function loadConfig(): void
    {
        require_once 'Helpers/PhpHelpers.php';

        $config_loader = ConfigLoaderFactory::create();

        Path::loadConfig($config_loader);
        Connexion::loadConfig($config_loader);

        Router::loadRoutes();
    }
}