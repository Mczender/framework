<?php

namespace Core\Form\Builder;

use Core\Form\Fields\AbstractField;
use Core\Utils\Collections\Collection;

class FormBuilder 
{
    private Collection $fields;
    
    public function __construct()
    {
        $this->fields = new Collection(AbstractField::class);
    }

    public function add(string $name, string $type, array $options = []): self
    {
        $field = new $type($name, $options);

        $this->fields->add($field);

        return $this;
    }
}