<?php
namespace Core\Form;

use Core\Form\Builder\FormBuilder;

abstract class AbstractForm 
{
    public abstract function buildForm(FormBuilder $builder, array $options): void;
}