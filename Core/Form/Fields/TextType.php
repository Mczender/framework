<?php

namespace Core\Form\Fields;

class TextType extends AbstractField
{
    protected function configureOptions(): array
    {
        return [];
    }

    protected function setDefaultOptions(): array
    {
        return [];
    }
}