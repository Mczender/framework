<?php

namespace Core\Form\Fields;

use BadTypeFieldException;
use Core\Form\Options\OptionInterface;
use Core\Form\Options\DataOption;
use Core\Form\Options\LengthOption;
use Core\Form\Options\RequiredOption;

abstract class AbstractField
{
    const AVAILABLES_TYPES = [
        'text',
        'textarea',
        'number',
        'email',
        'password',
        'button',
        'checkbox',
        'color',
        'date',
        'datetime-local',
        'email',
        'file',
        'hidden',
        'image',
        'number',
        'radio',
        'submit',
        'tel',
        'url'
    ];

    private $availables_options = [
        'required'      => RequiredOption::class,
        'data'          => DataOption::class,
        'length'        => LengthOption::class,
        'label'         => "",
        'help'          => null,
        'empty_data'    => null,
        'attrs'         => [],
    ];

    private array $options = [];
    
    /**
     * TODO : Voir à partir de ça comment faire pour redéfinir les valeurs par défauts des options pour depuis une classe Field
     * => Ajouter l'utilisation de la méthod setDefaultOption à la logique ci-dessous
     */
    public function __construct(private string $name, array $options = [], private string $type = 'text')
    {
        if (!in_array($this->type, self::AVAILABLES_TYPES)) {
            throw new BadTypeFieldException('"' . $this->type . '" n\'est pas un type de champ valide', 500);
        }
        
        foreach ($this->getSpecificOptions() as $specificOption => $optionClass) {
            $optionValidator = new $optionClass();
            $this->initOption($optionValidator, $specificOption, $options);
        }
    }

    private function initOption(OptionInterface $optionValidator, string $specificOption, array $options): void
    {
        $valueOption = $options[$specificOption] ?? $this->setDefaultOptions()[$specificOption] ?? $optionValidator->getDefault();

        $this->addOption($optionValidator, $valueOption);
    }

    /**
     * Permet de définir des options spécifiques à un champ avec son validateur
     */
    protected abstract function configureOptions(): array;

    /**
     * Permet de surcharger les valeurs par défaut des options
     * TODO : à mettre en oeuvre dans la logique du constructeur
     */
    protected abstract function setDefaultOptions(): array;

    public function getType(): string
    {
        return $this->type;
    }

    public function getOption(string $option): mixed
    {
        return $this->options[$option];
    }

        
    /**
     *
     * @param  mixed $option
     * @param  mixed $value
     * @return self
     */
    public function setOption(string $option, mixed $value): self
    {
        if (array_key_exists($option, $this->getSpecificOptions())) {
            $optionValidatorClass = $this->getSpecificOptions()[$option];
            $optionValidator = new $optionValidatorClass();

            $this->addOption($optionValidator, $value);
        }
        
        return $this;
    }
            
    /**
     * Permet d'ajouter une option à la configuration si sa valeur est valide
     *
     * @param  mixed $option
     * @param  mixed $value
     * @return void
     */
    private function addOption(OptionInterface $option, mixed $value)
    {
        if ($option->isValid($value)) {
            $this->options[$option] = $value;
        }
    }

    /**
     * Permet d'ajouter des options spécifique à un champ
     *
     * @return array
     */
    private function getSpecificOptions(): array
    {
        return array_merge($this->availables_options, $this->configureOptions());
    }
}