<?php
namespace Core\Form;

use BadFormClassExepction;
use Core\Form\Builder\FormBuilder;
use Core\Form\Form;
use Core\Table\AbstractTable;

class FormFactory 
{
    public function createForm(string $formClass, ?AbstractTable $data, array $options = []): Form
    {
        $formBuilder = new FormBuilder();
        $form = new $formClass(); // TODO : Voir pour l'injection de dépendances dans le constructeur
        
        if (!($form instanceof AbstractForm)) {
            throw new BadFormClassExepction('La classe "' . $formClass . '" n\'est pas une classe formulaire valide. Elle doit étendre de AbstractForm"', 500);
        }
        
        $form->buildForm($formBuilder, $options);
    }
}