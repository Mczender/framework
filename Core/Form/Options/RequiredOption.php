<?php
namespace Core\Form\Options;

use Core\Form\Options\OptionInterface;

class RequiredOption implements OptionInterface
{
    public function isValid(mixed $value): bool
    {
        return is_bool($value) || is_null($value);
    }

    public function getDefault(): mixed
    {
        return false;
    }
}