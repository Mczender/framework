<?php
namespace Core\Form\Options;

class LengthOption implements OptionInterface
{
    public function getDefault(): int
    {
        return 255;
    }

    public function isValid(mixed $value): bool
    {
        return $value === null || is_int($value);
    }
}
