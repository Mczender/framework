<?php

namespace Core\Form\Options;

interface OptionInterface
{
    public function isValid(mixed $value): bool;

    public function getDefault(): mixed;
}