<?php
namespace Core\Form\Options;

use Core\Form\Options\OptionInterface;

class DataOption implements OptionInterface
{
    public function isValid(mixed $value): bool
    {
        return true;
    }

    public function getDefault(): mixed
    {
        return null;
    }
}