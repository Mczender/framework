<?php
namespace Core\Utils\Collections;

use Core\Utils\Collections\Exceptions\BadCollectionTypeException;
use Core\Utils\Collections\Exceptions\BadMatchCollectionTypeException;
use JetBrains\PhpStorm\Pure;

/**
 * Permet de gérer un tableau comme un objet
 * @template T
 */
class Collection
{
    const string TYPE_INT = 'int';
    const string TYPE_STRING = 'string';
    const string TYPE_BOOL = 'bool';
    const string TYPE_ARRAY = 'array';

    private ?string $type = null;
    private array $elements = [];

    /**
     * Créer une collection de donnée
     *
     * @param string|null $type Le type de données qui doit être accepté par la collection (null = tout type accepté)
     * @param array<T> $elements Les éléments à fournir à l'initialisation de la collection
     * @throws BadCollectionTypeException
     * @throws BadMatchCollectionTypeException
     */
    public function __construct(?string $type = null, array $elements = [])
    {
        $this->setType($type);

        if ($this->verifyElements($elements))
            $this->elements = $elements;
    }

    /**
     * Retourne le type accepté par la collection courante s'il est défini
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Permet de définir le type de données accepté par la collection
     *
     * @param string|null $type Le type de donnée (nom de classe ou type primaire)
     * @throws BadCollectionTypeException Si le type de données de demandé n'existe pas, une BadCollectionTypeException sera levée
     */
    public function setType(?string $type = null): void
    {
        if ($type !== null)
            (class_exists($type) || $this->isPrimaryType($type))
                ? $this->type = $type
                : throw new BadCollectionTypeException('Le type fournit à la collection n\'existe pas', 500)
            ;
        else
            $this->type = null;
    }

    /**
     * Permet de récupérer un élément par sa clé
     *
     * @param string $key La clé de l'élément à retourner
     * @return T Retourne l'élément. Retourne null si l'élément n'a pas été trouvé.
     */
    public function get(string $key): mixed
    {
        return $this->elements[$key] ?? null;
    }

    /**
     * Ajoute un élément sans clé à la collection
     *
     * @param T $element
     * @return $this
     * @throws BadMatchCollectionTypeException
     */
    public function add(mixed $element): self
    {
        if ($this->isTypeOfCollection($element))
            $this->elements[] = $element;
        else
            throw new BadMatchCollectionTypeException('L\'élément que vous tentez d\'ajouter à la collection ne correspond pas au type attendu par la collection. La collection attend des éléments de type "' . $this->getType() . '"', 500);

        return $this;
    }

    /**
     * @throws BadMatchCollectionTypeException
     */
    public function set(string|int $key, mixed $element): self
    {
        if ($this->isTypeOfCollection($element))
            $this->elements[$key] = $element;
        else
            throw new BadMatchCollectionTypeException('L\'élément que vous tentez d\'ajouter à la collection ne correspond pas au type attendu par la collection. La collection attend des éléments de type "' . $this->getType() . '"', 500);

        return $this;
    }

    /**
     * Détermine si un élément est déjà contenu dans la collection
     *
     * @param mixed $element
     * @return bool
     */
    public function contains(mixed $element): bool
    {
        return in_array($element, $this->elements);
    }

    /**
     * Détermine si une clé existe déjà dans la collection
     *
     * @param mixed $key
     * @return bool
     */
    public function has(mixed $key): bool
    {
        return array_key_exists($key, $this->elements);
    }

    /**
     * Supprime un élément de la collection par sa clé
     *
     * @param mixed $key La clé de l'élément à supprimer
     * @return bool Retourne false si l'élément n'a pas pu être supprimé, retourne true si non.
     */
    public function remove(mixed $key): bool
    {
        if (array_key_exists($key, $this->elements))
            unset($this->elements[$key]);
        else
            return false;

        return true;
    }

    /**
     * Supprime l'élément donné de la collection
     *
     * @param T $element L'élément à supprimer de la collection
     * @return bool Retourne false si l'élément n'a pu être supprimé, retourne true si non.
     */
    public function removeElement(mixed $element): bool
    {
        if (false !== $key = array_search($element, $this->elements, true))
            unset($this->elements[$key]);
        else
            return false;

        return true;
    }

    /**
     * @param Collection<T> $collection
     * @return $this
     */
    public function merge(Collection $collection): self
    {
        if ($collection->getType() === $this->getType())
            $this->elements = array_merge($this->elements, $collection->toArray());

        return $this;
    }

    public function isEmpty(): bool
    {
        return empty($this->elements);
    }

    /**
     * Retourne les éléments de la collection sous forme de tableau
     * @return array<T>
     */
    public function toArray(): array
    {
        return $this->elements;
    }

    /**
     * Permet de savoir si un élément est de type attendu par la collection
     * Le type peut être défini au moment de l'instanciation (constructeur) ou via la méthode Collection::getType()
     *
     * @param T $element
     * @return bool Retourne true si l'élément est du type défini ou si aucun type n'a été défini. Retourne false si l'élément ne correspond pas au type défini
     */
    #[Pure]
    public function isTypeOfCollection(mixed $element): bool
    {
        if ($this->type !== null) {
            if ($this->isPrimaryType($this->type))
                return
                    !(($this->type === self::TYPE_INT && !is_int($element))
                        ||
                        ($this->type === self::TYPE_BOOL && !is_bool($element))
                        ||
                        ($this->type === self::TYPE_STRING && !is_string($element))
                        ||
                        ($this->type === self::TYPE_ARRAY && !is_array($element)))
                    ;
            elseif (!($element instanceof $this->type))
                return false;

        } //if
        return true;
    }

    /**
     * Permet de vérifier si le type fournit est primaire ou non
     *
     * @param string|null $type Le type sous forme de string
     * @return bool Retourne true si le type est primaire, false si non.
     */
    private function isPrimaryType(?string $type): bool
    {
        return match ($type) {
            self::TYPE_INT,
            self::TYPE_STRING,
            self::TYPE_BOOL,
            self::TYPE_ARRAY
                => true,
            default => false
        };
    }

    /**
     * Vérifie si les éléments de la collection ont le bon type
     *
     * @param array<T> $elements
     * @return bool
     * @throws BadMatchCollectionTypeException
     */
    private function verifyElements(array $elements): bool
    {
        foreach ($elements as $key => $element)
            if (!$this->isTypeOfCollection($element))
                throw new BadMatchCollectionTypeException(
                    'Un des éléments fournit à l\'instanciation 
                    de la collection ("' . $element . '" à la clé ' . $key . ') ne correspond pas au type accepté (' . $this->type . ')',
                    500
                );

        return true;
    }
}