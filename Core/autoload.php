<?php

use Core\Config\Path\Path;
use Core\Exceptions\NotFoundedClassException;

require_once 'Config/Path/Path.php';

spl_autoload_register(function($class) {
    $explodedClass = explode('\\', $class);

    $namespace = '';
    $className = '';
    foreach ($explodedClass as $key => $partClass)
    {
        if ($key + 1 < sizeof($explodedClass)) {
            $namespace .= $partClass . (($key + 1 < sizeof($explodedClass) - 1)  ? '\\' : '');
        } else {
            $className = $partClass;
        }
    }

    if (false !== $module = array_search($namespace, Path::getAllNamespaces())) {
        $namespace = convert_directory_separator_in_path(Path::getAllPaths()[$module]);
    } else {
        $namespace = dirname(__DIR__) . DIRECTORY_SEPARATOR . $namespace;
    }

    $file = $namespace . DIRECTORY_SEPARATOR . $className . '.php';

    if (file_exists($file)) {
        require_once $file ;
    } else {
        throw new NotFoundedClassException("Impossible de charger la classe: $class", 500);
    }
});


