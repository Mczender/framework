<?php

use JetBrains\PhpStorm\NoReturn;

/**
 * Permet de debugger des variables sans interrompre execution du programme
 *
 * @param mixed ...$values
 */
function dump(mixed ...$values): void
{
    $infos = [];
    $stack_trace = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 2);

    if ($stack_trace[1]['function'] === 'dd')
        $stack_trace = $stack_trace[1];
    else
        $stack_trace = $stack_trace[0];

    $class = sub_string($stack_trace['file'], 'Core');
    $line = $stack_trace['line'];

    if (is_array($values[0])) $values = $values[0];

    foreach ($values as $val) {
        require \Core\Config\Path\Path::getCoreDirectory() . DIRECTORY_SEPARATOR . 'Debugger' . DIRECTORY_SEPARATOR . 'Template' . DIRECTORY_SEPARATOR . 'dumper_template.php';
    }
}

/**
 * Permet de debugger des variables et stop l'exécution du programme
 *
 * @param mixed ...$values
 */
#[NoReturn]
function dd(mixed ...$values): void
{
    dump($values);
    die;
}