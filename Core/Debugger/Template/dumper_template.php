<div class="debug">
<p><?= $class . ' à la ligne ' . $line?></p>
    <pre>
<?php var_dump($val); ?>
    </pre>
</div>

<style>
    .debug {
        background-color: #242424;
        color: #b387ff;
    }

    .debug p {
        font-weight: bold;
    }
</style>