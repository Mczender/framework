<?php

namespace Core\Parser;

use Core\Exceptions\UnloadedLibraryException;

final class YAML implements Parser
{
    private array $parsed_file;

    public function __construct(private ?string $file_path = null)
    {
        $this->read($this->file_path);
    }

    public static function read_to_array(string $file): array
    {
        return Spyc::YAMLLoad($file);
    }

    public function read(string $file): Parser
    {
        $this->file_path = $file;
        $this->parsed_file = self::read_to_array($this->file_path);

        return $this;
    }

    public function get(mixed $key): mixed
    {
        return $this->parsed_file[$key] ?? null;
    }

    public function has(mixed $key): bool
    {
        return array_key_exists($key, $this->parsed_file);
    }
}