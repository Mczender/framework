<?php
namespace Core\Parser;

interface Parser {
    public static function read_to_array(string $file): array;

    public function read(string $file): self;

}
