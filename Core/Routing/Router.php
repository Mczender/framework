<?php
namespace Core\Routing;

use Core\Controller\Exceptions\NotControllerFindException;
use Core\Controller\Exceptions\NotMethodFindException;
use Core\Routing\Exceptions\NotRouteFindException;
use Core\Routing\Loader\Attributs;
use Core\Routing\Loader\YAML;
use Core\Routing\Routes\Parameter;
use Core\Routing\Routes\Route;
use Core\Utils\Collections\Collection;

final class Router {

    private static Collection $routes;

    public function __construct()
    {
        self::$routes = new Collection(Route::class);
    }

    /**
     * Permet de charger toutes les routes définies dans la configuration paramétrée
     *
     */
    public static function loadRoutes(): void
    {
        //TODO : Utiliser le loader paramétré dans le fichier de configuration du projet
        foreach([(new YAML()), (new Attributs())] as $routeParser) {
            self::$routes->merge($routeParser->loadRoutes()); //TODO : Vérifier que ça fonctionne bien une fois toutes les méthodes de récupération implémentées
        }
    }

    /**
     * Permet de faire matcher la requête de l'utilisateur avec une des routes définie
     *
     * @throws Routes\Exceptions\RouteParameterException
     * @throws NotRouteFindException
     */
    public function match() : void
    {
        $requested_uri = $this->getRequestedParsedURI();

        /** @var Route $route */
        $match_route = false;
        foreach (self::$routes->toArray() as $route) {
            $match_route = true;
            if (sizeof($route->parsedPath) === sizeof($requested_uri) && str_starts_with($this->getRequestedURI(), $route->baseUri)) {
                //L'URL contient des paramètres
                if ($route->hasParameters()) {
                    foreach ($requested_uri as $sub_position => $sub_uri) {
                        if (!$sub_uri === $route->getUriPartByPosition($sub_position) && !$route->getParameter($sub_position) !== null) {
                            $match_route = false;
                            break;
                        }
                    }

                    if ($match_route) {
                        $match_route = $route;
                    }
                } elseif ($this->getRequestedURI() === $route->path) {
                    $match_route = $route;
                    break;
                } else {
                    $match_route = false;
                }
            } else {
                $match_route = false;
            }
        } //foreach

        if ($match_route) {
            $controller = $match_route->controller;
            $controller = new $controller();
            $methode = $match_route->methode;
            $controller->$methode(...$this->getRequestedUriParameters($match_route->parameters));
        } else {
            throw new NotRouteFindException('Aucune route n\'a été trouvée pour la page "' . $this->getRequestedURI() . '"', 404);
        }
    }

    /**
     * Récupère les valeurs des paramètres de l'URI s'ils sont valides
     *
     * @param Collection<Parameter> $defined_parameters Collection des paramètres d'une route
     * @return array
     * @throws Routes\Exceptions\RouteParameterException
     */
    private function getRequestedUriParameters(Collection $defined_parameters): array
    {
        $parameters = [];
        $requested_uri = $this->getRequestedParsedURI();

        /** @var Parameter $parameter */
        foreach ($defined_parameters->toArray() as $parameter) {
            if ($parameter->testParameterValue($requested_uri[$parameter->position])) {
                $parameters[] = $requested_uri[$parameter->position];
            }
        }
        
        return $parameters;
    }

    private function getRequestedURI(): string
    {
        return $_SERVER['REQUEST_URI'];
    }

    private function getRequestedParsedURI(): array
    {
        if ($_SERVER['REQUEST_URI'] === '/')
            return ['/'];

        return array_filter(explode('/', $_SERVER['REQUEST_URI']), function ($element) {
            return !empty($element);
        });
    }
}