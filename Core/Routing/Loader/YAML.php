<?php

namespace Core\Routing\Loader;

use Core\Config\Path\Path;
use Core\Controller\Exceptions\NotControllerFindException;
use Core\Controller\Exceptions\NotMethodFindException;
use Core\Routing\Parser\Parser;
use Core\Routing\Routes\Exceptions\RouteConfigException;
use Core\Routing\Routes\Route;
use Core\Routing\Validator\AbstractValidator;
use Core\Routing\Validator\YAML as YamlValidator;
use Core\Routing\Parser\Exceptions\RouteYAMLFileException;
use Core\Routing\Routes\Exceptions\RouteFormatExcpetion;
use Core\Utils\Collections\Collection;
use Core\Utils\Collections\Exceptions\BadMatchCollectionTypeException;

final class YAML extends Loader implements RouteLoader
{
    /**
     * @return Collection<Route>
     * @throws NotControllerFindException
     * @throws NotMethodFindException
     * @throws RouteConfigException
     * @throws RouteFormatExcpetion
     * @throws RouteYAMLFileException|BadMatchCollectionTypeException
     */
    public function loadRoutes(): Collection
    {
        $routes_file = $this->getRoutesFile();

        $routes = new Collection(Route::class);
        foreach ($routes_file['routing'] as $routeName => $routeConfig) {
            $validator = new YamlValidator($routeName, $routeConfig);
            $routes->add($this->loadRouteConfig($routeName, $routeConfig, $validator));
        }

        return $routes;
    }

    /**
     * Construit un objet de type Route si sa configuration est valide
     *
     * @param string $routeName Le nom de la route à construire
     * @param array $routeConfig La configuration de la route
     * @param Validator $validator Le validateur permettant de valider la configuration en fonction de l'environnement
     * @return Route|null
     * @throws NotControllerFindException
     * @throws NotMethodFindException
     * @throws BadMatchCollectionTypeException
     */
    public function loadRouteConfig(string $routeName, array $routeConfig, AbstractValidator $validator): ?Route
    {
        if ($validator->isValid()) {
            return new Route(
                $routeName,
                $routeConfig['path'],
                Parser::getParsedURI($routeConfig['path']),
                Parser::getBaseURI($routeConfig['path']),
                Parser::getParsedBaseURI($routeConfig['path']),
                $this->loadRouteParameters($routeConfig['path'], $routeConfig['requirements'] ?? []),
                Parser::parseControllerClass($routeConfig['controller']),
                Parser::parseMethodeController($routeConfig['controller'], $routeConfig['action'])
            );
        }
        return null;
    }

    /**
     * @throws RouteYAMLFileException
     */
    private function getRoutesFile(): array
    {
        if (!file_exists(Path::getRoutesYamlFile()))
            throw new RouteYAMLFileException('Le fichier de configuration de route YAML (' . Path::getRoutesYamlFile() . ') n\'existe pas !', 500);

        return \Core\Parser\YAML::read_to_array(Path::getRoutesYamlFile());
    }
}