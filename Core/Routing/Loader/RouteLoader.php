<?php

namespace Core\Routing\Loader;

use Core\Routing\Routes\Route;
use Core\Routing\Validator\AbstractValidator;
use Core\Utils\Collections\Collection;

interface RouteLoader
{
    public function loadRoutes(): Collection;

    public function loadRouteConfig(string $routeName, array $routeConfig, AbstractValidator $validator): ?Route;
}