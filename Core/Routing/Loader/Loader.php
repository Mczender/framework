<?php

namespace Core\Routing\Loader;

use Core\Routing\Parser\Parser;
use Core\Routing\Routes\Parameter;
use Core\Routing\Routes\Requirement;
use Core\Utils\Collections\Collection;
use Core\Utils\Collections\Exceptions\BadMatchCollectionTypeException;

abstract class Loader
{
    /**
     * @param string $path
     * @param array $requirements
     * @return Collection<Parameter>
     * @throws BadMatchCollectionTypeException
     */
    protected function loadRouteParameters(string $path, array $requirements): Collection
    {
        $parameters = new Collection(Parameter::class);

        $route_parameters = Parser::getParametersURI($path);

        if (!empty($requirements))
            $route_requirements = Parser::getRequirementsConfig($requirements);

        foreach ($route_parameters as $position => $parameter_name) {
            $requirements_collection = new Collection(Requirement::class);
            if (isset($route_requirements) && isset($route_requirements[$parameter_name]))
                foreach ($route_requirements[$parameter_name] as $requirement)
                    $requirements_collection->add(new Requirement($requirement['type'] ?? null, $requirement['length'] ?? null, $requirement['regex'] ?? null));

            $parameters->add(new Parameter($parameter_name, $position, $requirements_collection));
        }

        return $parameters;
    }
}