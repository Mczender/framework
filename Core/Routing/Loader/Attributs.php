<?php

namespace Core\Routing\Loader;

use Core\Config\Path\Path;
use Core\Controller\Exceptions\NotControllerFindException;
use Core\Controller\Exceptions\NotMethodFindException;
use Core\Routing\Parser\Parser;
use Core\Routing\Routes\Exceptions\RouteConfigException;
use Core\Routing\Routes\Exceptions\RouteFormatExcpetion;
use Core\Routing\Routes\Route;
use Core\Routing\Routes\Attributs\Route as routeAttribut;
use Core\Routing\Validator\Attribut;
use Core\Routing\Validator\AbstractValidator;
use Core\Utils\Collections\Collection;
use Core\Utils\Collections\Exceptions\BadMatchCollectionTypeException;

final class Attributs extends Loader implements RouteLoader
{
    /**
     * @return Collection
     * @throws BadMatchCollectionTypeException
     * @throws NotControllerFindException
     * @throws NotMethodFindException
     * @throws RouteConfigException
     * @throws RouteFormatExcpetion
     */
    public function loadRoutes(): Collection
    {
        $routes = new Collection(Route::class);

        /** @var $route_attribut $route_attribut */
        foreach ($this->getAttributs()->toArray() as $route_attribut) {
            $validator = new Attribut($route_attribut->name, $route_attribut);
            $routes->add($this->loadRouteConfig($route_attribut->name, $route_attribut, $validator));
        }

        return $routes;
    }

    /**
     * @throws NotMethodFindException
     * @throws NotControllerFindException
     * @throws BadMatchCollectionTypeException
     */
    public function loadRouteConfig(string $routeName, array|object $routeConfig, AbstractValidator $validator): ?Route
    {
        if ($validator->isValid()) {
            return new Route(
                $routeName,
                $routeConfig->path,
                Parser::getParsedURI($routeConfig->path),
                Parser::getBaseURI($routeConfig->path),
                Parser::getParsedBaseURI($routeConfig->path),
                $this->loadRouteParameters($routeConfig->path, $routeConfig->requirements),
                Parser::parseControllerClass($routeConfig->getController()),
                Parser::parseMethodeController($routeConfig->getController(), $routeConfig->getAction())
            );
        }
        return null;
    }

    /**
     * Récupère l'ensemble des attributs de routes des contrôleurs et les retournes dans une collection.
     *
     * @return Collection<routeAttribut>
     * @throws BadMatchCollectionTypeException
     */
    public function getAttributs(): Collection
    {
        $attributs = new Collection(routeAttribut::class);

        foreach (new \DirectoryIterator(Path::getControllerDirectory()) as $fileInfo) {
            if ($fileInfo->isFile()) {
                $controller_class = Path::getControllerNamespace() . DIRECTORY_SEPARATOR . substr($fileInfo->getFilename(), 0, -4);
                if (class_exists($controller_class)) {
                    $reflection = new \ReflectionClass($controller_class);
                    foreach ($reflection->getMethods() as $reflectionMethod) {
                        foreach ($reflectionMethod->getAttributes() as $reflectionAttribute) {
                            if ($reflectionAttribute->getName() === routeAttribut::class) {
                                /** @var routeAttribut $route */
                                $route = ($reflectionAttribute->newInstance())->setController($controller_class)->setAction($reflectionMethod->getName());
                                $attributs->add($route);
                            } //if
                        } //foreach
                    } //foreach
                } //if
            } //if
        } //foreach
        return $attributs;
    }
}