<?php

namespace Core\Routing\Validator;

use Core\Routing\Parser\Parser;
use Core\Routing\Routes\Exceptions\RouteConfigException;
use Core\Routing\Routes\Exceptions\RouteFormatExcpetion;

abstract class AbstractValidator
{
    const AVAILABLE_PROPERTIES = ['path', 'controller', 'action', 'methods', 'requirements', 'defaults', 'name'];

    private bool $is_valid;

    /**
     * @param string $route_name
     * @param array|object $route_config
     * @throws RouteConfigException
     * @throws RouteFormatExcpetion
     */
    public function __construct(string $route_name, array|object $route_config)
    {
        $this->is_valid =
            $this->configIsValid($route_name, $route_config)
            &&
            $this->routeNameIsValid($route_name)
            &&
            $this->routePathIsValid($route_name, $route_config)
            &&
            $this->routeControllerIsValid($route_name, $route_config)
            &&
            $this->routeActionIsValid($route_name, $route_config)
            &&
            $this->routeRequirementsIdValid($route_name, $route_config)
        ;
    }

    /**
     * Vérifie si les propriétés de la configuration d'une route sont valides
     *
     * @param string $route_name
     * @param array|object $route_config
     * @return bool
     * @throws RouteConfigException
     */
    private function configIsValid(string $route_name, array|object $route_config): bool
    {
        foreach ($route_config as $property => $config) {
            if (!in_array($property, self::AVAILABLE_PROPERTIES)) {
                $to_string_parameters = RouteConfigException::getAvailableParametersToString(self::AVAILABLE_PROPERTIES);
                throw new RouteConfigException('La route "' . $route_name . '" est mal configurée ! Le paramètre "' . $property . '" est inconnu. Les propriétés disponibles sont : ' . $to_string_parameters, 500);
            }
        }
        return true;
    }

    /**
     * Vérifie que le nom d'une route définie est valide.
     *
     * @param string $routeName
     * @return bool
     * @throws RouteFormatExcpetion
     */
    private function routeNameIsValid(string $routeName): bool
    {
        if (!(strlen($routeName) > 1))
            throw new RouteFormatExcpetion("Nom de route mal formaté !");

        return true;
    }

    /**
     * Vérifie si l'URL fournie pour la définition d'une route est valide
     *
     * @param string $routeName
     * @param array|object $routeConfig
     * @return bool
     */
    protected abstract function routePathIsValid(string $routeName, array|object $routeConfig): bool;

    /**
     * Vérifie si le contrôleur fourni pour la définition d'une route est valide
     *
     * @param string $routeName
     * @param array|object $routeConfig
     * @return bool
     */
    protected abstract function routeControllerIsValid(string $routeName, array|object $routeConfig): bool;

    /**
     * Vérifie si la méthode fournie pour la définition d'une route est valide
     *
     * @param string $routeName
     * @param array|object $routeConfig
     * @return bool
     */
    protected abstract function routeActionIsValid(string $routeName, array|object $routeConfig): bool;

    /**
     * Vérifie si les conditions de validations pour les paramètres d'une route sont valides.
     *
     * @param string $routeName
     * @param array|object $routeConfig
     * @return bool
     */
    protected abstract function routeRequirementsIdValid(string $routeName, array|object $routeConfig): bool;

    /**
     * Vérifie si une configuration de route en YAML est valide
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->is_valid;
    }

    /**
     * Test si les paramètres définis dans un URI sont uniques
     *
     * @param string $route_name
     * @param string $route_path
     * @return bool
     * @throws RouteConfigException
     */
    protected function definedParametersAreUnique(string $route_name, string $route_path): bool
    {
        foreach (Parser::getParametersURI($route_path) ?? [] as $parameter_name) {
            if (!is_unique_value_array($parameter_name, Parser::getParametersURI($route_path)))
                throw new RouteConfigException('Le paramètre d\'URL "' . $parameter_name . '" a été définit plus d\'une fois dans le paramètre "path" de la route "' . $route_name . '". ', 500);
        }
        return true;
    }
}