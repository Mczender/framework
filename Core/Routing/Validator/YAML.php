<?php
namespace Core\Routing\Validator;

use Core\Controller\Exceptions\NotControllerFindException;
use Core\Routing\Parser\Parser;
use Core\Routing\Routes\Exceptions\RouteConfigException;
use Core\Routing\Routes\Exceptions\RouteFormatExcpetion;
use Core\Routing\Routes\Requirement;

final class YAML extends AbstractValidator
{
    /**
     * Vérifie si le paramètre path de la définition d'une route est valide.
     *
     * @param string $routeName
     * @param array|object $routeConfig
     * @return bool
     * @throws RouteConfigException
     * @throws RouteFormatExcpetion
     */
    public function routePathIsValid(string $routeName, array|object $routeConfig): bool
    {
        if (!isset($routeConfig['path']))
            throw new RouteFormatExcpetion('Le paramètre "path" de la route ' . $routeName . ' est manquant ! ', 500);

        if (!(strlen($routeConfig['path']) > 0))
            throw new RouteFormatExcpetion('Le paramètre "path" de la route ' . $routeName . ' est vide ', 500);

        return $this->definedParametersAreUnique($routeName, $routeConfig['path']);
    }

    /**
     * Vérifie si le paramètre 'controller' de la définition d'une route est valide
     *
     * @param string $routeName
     * @param array|object $routeConfig
     * @return bool
     * @throws RouteFormatExcpetion|NotControllerFindException|RouteConfigException
     */
    public function routeControllerIsValid(string $routeName, array|object $routeConfig): bool
    {
        if (!isset($routeConfig['controller']))
            throw new RouteFormatExcpetion('Le paramètre "controller" de la route "' . $routeName . '" est manquant ! ', 500);

        if (!class_exists($routeConfig['controller']))
            throw new NotControllerFindException('Le contrôleur "' . $routeConfig['controller'] . '" pour la route "' . $routeName . '" n\'a pas été trouvé ! Utilisez vous le bon namespace ? ', 500);

        if (Parser::controllerClassContainMethode($routeConfig['controller']) && isset($routeConfig['action']) && $routeConfig['action'])
            throw new RouteConfigException('La méthode à atteindre à été renseignée 2 fois pour la route "' . $routeName . '" ! Renseignez la soit avec le paramètre "controller", soit dans le paramètre "action".');

        return true;
    }

    /**
     * Vérifie si le paramètre 'methode' de la définition d'une route est valide.
     *
     * @param string $routeName
     * @param array|object $routeConfig
     * @return bool
     * @throws RouteFormatExcpetion
     */
    public function routeActionIsValid(string $routeName, array|object $routeConfig): bool
    {
        if (!isset($routeConfig['action']) && !Parser::controllerClassContainMethode($routeConfig['controller']))
            throw new RouteFormatExcpetion('Le paramètre "action" de la route ' . $routeName . ' est manquant ! ', 500);

        return true;
    }

    /**
     * Vérifie si les conditions de validations pour les paramètres d'une route sont valides.
     *
     * @param string $routeName
     * @param array|object $routeConfig
     * @return bool
     * @throws RouteConfigException
     */
    public function routeRequirementsIdValid(string $routeName, array|object $routeConfig): bool
    {
        //Si le paramètre requirements n'est pas définie, alors la configuration est valide (le paramètre est optionnel)
        if (!isset($routeConfig['requirements']))
            return true;

        else {
            $requirements = Parser::getRequirementsConfig($routeConfig['requirements']);
            foreach($requirements as $parameter => $config) {
                if (!in_array($parameter, Parser::getParametersURI($routeConfig['path'])))
                    throw new RouteConfigException(
                        'La route "' . $routeName . '" est mal configurée ! 
                        Le paramètre "requirements" configure un paramètre de route nommé "' . $parameter . '" qui n\'a pas été configuré dans l\'URI (path) de la route. 
                        Ajoutez le paramètre ou retirez le requirement'
                        ,
                        500
                    );

                if (isset($config['regex']) && preg_match($config['regex'], '') === false)
                    throw new RouteConfigException(
                        'La route "' . $routeName . '" est mal configurée ! La regex donnée dans le paramètre "requirement" pour le l\'argument de route "' . $parameter . '" 
                        est mal configurée.',
                        500
                    );

                elseif (!isset($config['regex'])) {
                    foreach ($config as $type_args) {
                        if (!in_array($type_args['type'], Requirement::getAvailableType()))
                            throw new RouteConfigException(
                                'La route "' . $routeName . '" est mal configurée ! 
                                Le type de données donné dans le paramètre "requirements" n\'existe pas pour l\'argument de route "' . $parameter . '". 
                                Les types de données possibles sont : ' . RouteConfigException::getAvailableParametersToString(['string', 'int', 'null'])
                            );
                        if (isset($type_args['length']) && !is_int($type_args['length']))
                            throw new RouteConfigException(
                                'La route "' . $routeName . '" est mal configurée !
                                La taille maximale donnée dans le paramètre requirements est invalide pour l\'argument de route "' . $parameter . '" ! Un chiffre entier doit être donné.',
                                500
                            );
                    } //if
                } //elseif
            } //foreach
        } //else
        return true;
    }
}