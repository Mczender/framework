<?php

namespace Core\Routing\Routes\Exceptions;

class RouteConfigException extends \Core\Exceptions\FrameworkException
{
    public static function getAvailableParametersToString(array $available_parameters): string
    {
        $string_parameters = "<br>";
        foreach ($available_parameters as $parameter) {
            $string_parameters .= " - " . $parameter . '<br>';
        }

        return $string_parameters;
    }
}