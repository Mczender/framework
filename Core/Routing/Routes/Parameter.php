<?php

namespace Core\Routing\Routes;

use Core\Utils\Collections\Collection;

class Parameter
{
    /**
     * @param string $name
     * @param int $position
     * @param Collection<Requirement> $requirements
     */
    public function __construct(
        public readonly string $name,
        public readonly int $position,
        public readonly Collection $requirements,
    )
    {
    }

    /**
     * Test si une valeur correspond au requirements du paramètre
     *
     * @param mixed $value La valeur a tester
     * @return bool Retourne true si la valeur correspond au moins à un requirement, false si non.
     * @throws Exceptions\RouteParameterException
     */
    public function testParameterValue(mixed $value): bool
    {
        /** @var Requirement $requirement */
        if ($this->requirements->isEmpty())
            return true;

        foreach ($this->requirements->toArray() as $requirement) {
            if ($requirement->testValue($this->name, $value)) {
                return true;
            }
        }
        return false;
    }
}