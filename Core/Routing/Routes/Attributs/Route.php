<?php

namespace Core\Routing\Routes\Attributs;

#[\Attribute(\Attribute::TARGET_METHOD)]
class Route
{
    private ?string $controller = null;
    private ?string $action = null;

    public function __construct(
        public readonly string $name,
        public readonly string $path,
        public readonly array $methods = [],
        public readonly array $requirements = [],
        public readonly array $defaults = []
    )
    {
    }

    public function getController(): ?string
    {
        return $this->controller;
    }

    public function setController(string $controller): self
    {
        $this->controller = $controller;

        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }
}