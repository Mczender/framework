<?php
namespace Core\Routing\Routes;

use Core\Utils\Collections\Collection;
use JetBrains\PhpStorm\Pure;

class Route
{
    /**
     * @param string $name
     * @param string $path
     * @param array $parsedPath
     * @param string $baseUri
     * @param array $parsedBaseUri
     * @param Collection<Parameter> $parameters
     * @param string $controller
     * @param string $methode
     */
    public function __construct(
        public readonly string $name,
        public readonly string $path,
        public readonly array $parsedPath,
        public readonly string $baseUri,
        public readonly array $parsedBaseUri,
        public readonly Collection $parameters,
        public readonly string $controller,
        public readonly string $methode
    )
    {
    }

    public function getUriPartByPosition(int $position): ?string
    {
        return $this->parsedPath[$position] ?? null;
    }

    #[Pure]
    public function hasParameters(): bool
    {
        return !$this->parameters->isEmpty();
    }

    /**
     * Retourne un paramètre par rapport à sa position dans l'URI
     *
     * @param int $position
     * @return Parameter|null Retourne le paramètre trouvé ou null si aucun paramètre n'a été trouvé
     */
    public function getParameter(int $position): ?Parameter
    {
        /** @var Parameter $parameter */
        foreach ($this->parameters as $parameter) {
            if ($parameter->position === $position)
                return $parameter;
        }
        return null;
    }
}