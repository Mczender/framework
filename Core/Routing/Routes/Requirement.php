<?php

namespace Core\Routing\Routes;

use Core\Routing\Routes\Exceptions\RouteParameterException;

class Requirement
{
    const TYPE_INT = 'int';
    const TYPE_STRING = 'string';
    const TYPE_NULL = 'null';
    const TYPE_DOUBLE = 'double';

    public function __construct(public readonly ?string $type, public readonly ?int $length, public readonly ?string $regex)
    {
    }

    /**
     * Test si une valeur correspond au requirement
     *
     * Méthode appelée depuis la méthode testParameterValue de la classe Requirement
     *
     * @param string $name Le nom du paramètre
     * @param mixed $value La valeur a testé
     * @return bool Retourne true si la valeur correspond, si non, une RouteParameterException est levée
     * @throws RouteParameterException
     */
    public function testValue(string $name, mixed $value): bool
    {
        if ($this->regex !== null && preg_match($this->regex, $value) !== 1)
            throw new RouteParameterException('La valeur renseignée dans l\'URI pour le paramètre "' . $name . '" ne correspond pas au pattern configuré : "' . $this->regex . '"', 404);

        elseif ($this->type !== null) {
            $typeIsValid = match ($this->type) {
                'int' => is_int($value),
                'string' => is_string($value),
                'null' => is_null($value),
                'double' => is_double($value),
                default => false,
            };

            if (!$typeIsValid)
                throw new RouteParameterException('La valeur renseignée dans l\'URI pour le paramètre "' . $name . '" ne respecte pas le type configuré : "' . $this->type . '"', 404);

            if ($this->length !== null) {
                $lengthIsValid =
                    ($this->type === self::TYPE_STRING && strlen($value) <= $this->length)
                    ||
                    (in_array($this->type, [self::TYPE_INT, self::TYPE_DOUBLE]) && $value <= $this->length)
                ;

                if (!$lengthIsValid)
                    throw new RouteParameterException('La valeur renseignée dans l\'URI pour le paramètre "' . $name . '" est plug grand que la taille max configurée : "' . $this->length . '"', 404);
            }
        }

        return true;
    }

    public static function getAvailableType(): array
    {
        return [
            self::TYPE_INT, self::TYPE_STRING, self::TYPE_DOUBLE, self::TYPE_NULL
        ];
    }

}