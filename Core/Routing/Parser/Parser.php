<?php

namespace Core\Routing\Parser;

use Core\Controller\Exceptions\NotControllerFindException;
use Core\Controller\Exceptions\NotMethodFindException;

class Parser
{
    /**
     * Permet de parser une URL
     * Retourne un tableau avec les entrées suivantes :
     * - base_uri : la première partie static de l'URL
     * - parsed_uri : tableau contenant chaque partie de l'URI découpé par les '/'
     * - parameters : la liste des noms des paramètres définis (sous forme de tableau)
     * - parsed_base_uri : tableau contenant chaque partie du base_uri découpé par les '/'
     * - full_uri : l'URI complète sous forme de chaîne de caractères.
     *
     * @param string $uri
     * @return array
     */
    public static function parseUrl(string $uri): array
    {
        $array_filter = function ($element) {
            return !empty($element);
        };

        $parsed_url['parsed_uri'] = array_filter(explode('/', $uri), $array_filter);
        $parsed_url = array_merge($parsed_url, self::parseParametersUrl($parsed_url['parsed_uri']));

        $parsed_url['base_uri'] = '/';
        if (!empty($parsed_url['parameters'])) {
            for ($i = 0; $i < array_key_first($parsed_url['parameters']); $i++) {
                $parsed_url['base_uri'] .= explode('/', $uri)[$i] . (($i === array_key_first($parsed_url['parameters']) - 1) ? '/' : '');
            }
        }

        else
            $parsed_url['base_uri'] = $uri;

        if ($parsed_url['base_uri'] === '/') {
            $parsed_url['parsed_base_uri'] = ['/'];
            $parsed_url['parsed_uri'] = ['/'];
        } else {
            $parsed_url['parsed_base_uri'] = explode('/', $parsed_url['base_uri']);
            $parsed_url['parsed_base_uri'] = array_filter($parsed_url['parsed_base_uri'], $array_filter);
        }
        $parsed_url['full_uri'] = $uri;

        return $parsed_url;
    }

    /**
     * Retourne la partie statique de l'URL
     *
     * @param string $uri
     * @return string
     */
    public static function getBaseURI(string $uri): string
    {
        return self::parseUrl($uri)['base_uri'];
    }

    /**
     * Retourne l'URL parsée découpé par les '/'
     *
     * @param string $uri
     * @return array
     */
    public static function getParsedURI(string $uri): array
    {
        return self::parseUrl($uri)['parsed_uri'];
    }

    /**
     * Retourne la liste des noms des paramètres de l'URI
     *
     * @param string $uri
     * @return array
     */
    public static function getParametersURI(string $uri): array
    {
        return self::parseUrl($uri)['parameters'] ?? [];
    }

    /**
     * Retourne la partie statique de l'URI parsée découpée par des '/'
     *
     * @param string $uri
     * @return array
     */
    public static function getParsedBaseURI(string $uri): array
    {
        return self::parseUrl($uri)['parsed_base_uri'];
    }

    /**
     * Retourne le nom callable de la class d'un contrôleur. La classe peut être donnée sous différente forme lors de la déclaration d'une route :
     * - 'App\Controller\HomeController' : Nom complet
     * - 'App\Controller\HomeController::methode' : Nom complet + méthode
     * - 'homeController : Nom simple de la classe commençant ou non par une MAJ
     * - 'Home' : Nom simple de la classe sans l'extension 'Controller' (Les classe contrôleur doivent avoir l'extension) et avec ou sans MAJ
     *      - Si cette dernière méthode est utilisé, la classe doit impérativement se trouver dans le namespace par défaut configuré.
     *
     * Si la classe contrôleur n'est pas trouvée, une NotControllerFindException sera déclenchée.
     *
     * @param string $class_name
     * @return string
     * @throws NotControllerFindException
     */
    public static function parseControllerClass(string $class_name): string
    {
        if (class_exists($class_name))
            return $class_name;

        elseif (self::controllerClassContainMethode($class_name)) {

            $parsed_class = explode('::', $class_name)[0];

            if (class_exists($parsed_class))
                return $parsed_class;

            else
                throw new NotControllerFindException('Impossible de charger le controller spécifié : "' . $parsed_class . '". La classe existe-t-elle ? Utilisez vous le bon namespace ?', 500);

        } else {
            if (!str_ends_with($class_name, 'Controller'))
                $class_name .= 'Controller';
            //TODO: RENDRE LE NAMESPACE DYNAMIQUE VIA UN FICHIER DE CONFIG
            $full_class_name = 'App\\Controller\\' . ucfirst($class_name);

            if (class_exists($full_class_name))
                return $full_class_name;

            throw new NotControllerFindException('Impossible de charger le controller spécifié : "' . $class_name . '" recherché dans le namespace "App\\Controller\\".', 500);
        }
    }

    /**
     * Retourne le nom callable d'une méthode pour une classe Contrôleur. La méthode peut être passée de plusieurs manières lors de la déclaration d'une route :
     * - "App\Controller\HomeController::index" : La méthode est passée en même temps que la classe contrôleur (le même paramètre)
     * - "index" : Le nom de la méthode est passé dans un paramètre à part 'action'
     *
     * @param string $controller_class_name
     * @param string|null $methode
     * @return string
     * @throws NotControllerFindException
     * @throws NotMethodFindException
     */
    public static function parseMethodeController(string $controller_class_name, ?string $methode = null): string
    {
        $parsed_controller_class_name = self::parseControllerClass($controller_class_name);

        if (str_contains($controller_class_name, '::')) {
            $methode = explode('::', $controller_class_name)[1];

            if (method_exists($parsed_controller_class_name, $methode))
                return $methode;

            throw new NotMethodFindException('La méthode : "' . $methode . '" définie avec le contrôleur (même paramètre) n\'a pas été trouvé dans la classe : "' . $parsed_controller_class_name . '".', 500);

        } elseif (method_exists($parsed_controller_class_name, $methode))
            return $methode;

        throw new NotMethodFindException('La méthode : "' . $methode . '" n\'a pas été trouvé dans le contrôleur : "' . $parsed_controller_class_name . '".', 500);
    }

    /* HELPERS FONCTIONS */

    /**
     * Récupère lisiblement les noms des paramètres dans la définition d'une URL, et les retourne sous forme de tableau.
     *
     * @param array $parsedUrl
     * @return array
     */
    private static function parseParametersUrl(array $parsedUrl): array
    {
        //TODO : Ajouter une vérification du format (ne pas commencer par un chiffre, par de caractères spéciaux sauf '_')
        $parameters = [];

        foreach ($parsedUrl as $index => $parameter)
            if (preg_match('/^{[a-z]+.*}$/', $parameter) === 1)
                $parameters['parameters'][$index] = explode('}', explode('{', $parameter)[1])[0];

        return $parameters;
    }

    /**
     * Permet de savoir si la classe du contrôleur renseignée pour la configuration d'une route contient aussi le nom de la méthode à utiliser
     *
     * @param string $class_name
     * @return bool
     */
    public static function controllerClassContainMethode(string $class_name): bool
    {
        //TODO : AVOIR LE NAMESPACE EN DYNAMIQUE VIA UN FICHIER DE CONFIG
        return str_starts_with($class_name, 'App\\Controller\\') && str_contains($class_name,'::');
    }

    public static function getRequirementsConfig(array $routeRequirementsConfig): array
    {
        $requirements = [];
        foreach ($routeRequirementsConfig as $parameter => $config) {
            if (str_contains($config, '||'))
                $args_types = explode('||', $config);
            elseif (!str_starts_with($config, '/'))
                $args_types = [$config];
            else
                $requirements[$parameter]['regex'] = $config . (str_ends_with($config, '/') ? '' : '/');


            if (isset($args_types)) {
                foreach ($args_types ?? [] as $arg_type) {
                    if (str_contains($arg_type, '-')) {
                        $parsed_type = explode('-', $arg_type);
                        $requirements[$parameter][] = [
                            'type' => $parsed_type[0],
                            'length' => (int) $parsed_type[1]
                        ];
                    } else {
                        $requirements[$parameter][] = [
                            'type' => $arg_type,
                        ];
                    }
                }
            }
        }
        return $requirements;
    }
}