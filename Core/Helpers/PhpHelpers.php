<?php

/**
 * Fonction améliorer de subtr permettant de récupérer un segment de chaîne grâce à un séparateur.
 * La chaîne retournée comprend le séparateur donné en début de chaîne
 * @param string $string La chaîne à couper
 * @param string $separator Le séparateur à partir du quel la chaîne sera coupée (compris dans la valeur de retour)
 * @param int $offset Si la chaîne contient plusieurs fois le séparateur, permet d'indiquer à partir de quelle occurrence couper (le premier par défaut)
 * @param int|null $length La taille de la chaîne à retourner à partir du séparateur comme premier caractère.
 * @return string
 */
function sub_string(string $string, string $separator, int $offset = 0, ?int $length = null): string
{
    $separator_position = strpos($string, $separator, $offset);
    return substr($string, $separator_position, $length);
}

/**
 * Permet de savoir si une valeur apparait qu'une seule fois dans un tableau
 * @param mixed $value La valeur à rechercher
 * @param array $array Le tableau dans lequel chercher
 * @return bool Retourne true si la valeur n'apparait qu'une fois ou pas du tout, retourne false si la valeur apparait plus d'une fois
 */
function is_unique_value_array(mixed $value, array $array): bool
{
    $count_occurrence = 0;
    foreach ($array as $value_in_array) {
        if ($value === $value_in_array)
            $count_occurrence++;
    }

    return $count_occurrence <= 1;
}

/**
 * Permet d'adapter les séparateurs de répertoire dans un chemin.
 *
 * @param string $path
 * @return string
 */
function convert_directory_separator_in_path(string $path): string
{
    return  str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
}

function in_array_r(mixed $needle, array $haystack): bool
{
    foreach ($haystack as $item) {
        if ($item === $needle || (is_array($item) && in_array_r($needle, $item))) {
            return true;
        }
    }

    return false;
}

/**
 * Permet de savoir si un tableau est associatif
 *
 * @param array $array
 * @return bool
 */
function is_assoc(array $array): bool
{
    if ([] === $array) return false;
    return array_keys($array) !== range(0, count($array) - 1);
}