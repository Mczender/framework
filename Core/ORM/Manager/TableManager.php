<?php

namespace Core\Manager;

use Core\ORM\Table\AbstractTable;
use Core\Repository\AbstractRepository;

class TableManager
{
    public function persist(AbstractTable $table): self
    {
        return $this;
    }

    public function remove(AbstractTable $table): self
    {
        return $this;
    }

    public function flush(): self
    {
        return $this;
    }

    public function getRepository(AbstractTable|string $object_or_class): AbstractRepository
    {

    }
}