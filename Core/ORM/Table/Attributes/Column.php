<?php

namespace Core\Table\Attributes;

#[\Attribute]
class Column
{
    public function __construct(
        private ?string $name = null,
        private ?string $type = null,
        private ?int $length = null,
    )
    {
    }
}