<?php

namespace Core\Table\Attributes;

#[\Attribute]
class Table
{
    public function __construct(private ?string $name = null)
    {
    }
}