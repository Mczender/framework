<?php

namespace Core\Exception;

use Core\Exceptions\FrameworkException;

class NotSupportedDatabaseDriverException extends FrameworkException
{

}