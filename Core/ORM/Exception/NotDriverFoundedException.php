<?php

namespace Core\Exception;

use Core\Exceptions\FrameworkException;

class NotDriverFoundedException extends FrameworkException
{

}