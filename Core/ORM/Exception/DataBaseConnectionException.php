<?php

namespace Core\Exception;

use Core\Exceptions\FrameworkException;

class DataBaseConnectionException extends FrameworkException
{

}