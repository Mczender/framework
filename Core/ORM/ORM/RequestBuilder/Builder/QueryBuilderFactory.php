<?php

namespace Core\ORM\RequestBuilder\Builder;

use Core\Config\bdd\Connexion;
use Core\Config\Exceptions\NotBddConfigException;
use Core\Config\Exceptions\WrongYamlConfigException;
use Core\Config\Path\Path;
use Core\Exception\NotDriverFoundedException;
use Core\RequestBuilder\Builder\QueryBuilderInterface;
use Exception;

class QueryBuilderFactory
{
    /**
     * @throws WrongYamlConfigException
     * @throws NotBddConfigException
     * @throws Exception
     */
    public static function create(string $db_type, string $driver): QueryBuilderInterface
    {
        $version =  Connexion::getVersion();
        $driverClass = self::findDriverClass($db_type, $driver, $version);

        if ($driverClass) {
            return new $driverClass();
        } else {
            throw new NotDriverFoundedException("Aucun driver n'a été trouvé pour la version $version de $driver", 500);
        }
    }

    private static function findDriverClass(string $type, string $driver, string $version): ?string
    {
        $versionParts = explode('.', $version);
        $normalizedVersion = self::normalizeVersion($versionParts);

        $path_sep = DIRECTORY_SEPARATOR;
        $driverDir = Path::getCoreDirectory() . "{$path_sep}ORM{$path_sep}QueryBuilder{$path_sep}Builder{$path_sep}$type{$path_sep}$driver";
        $driverFiles = glob("$driverDir/*.php");

        $availableVersions = [];

        foreach ($driverFiles as $file) {
            if (preg_match("/{$driver}(\d+)QueryBuilder\.php$/", basename($file), $matches)) {
                $availableVersions[] = $matches[1];
            }
        }

        usort($availableVersions, 'version_compare');

        foreach (array_reverse($availableVersions) as $availableVersion) {
            if (version_compare($availableVersion, $normalizedVersion, '<=')) {
                $driverClass = "Core\\ORM\\QueryBuilder\\Builder\\$type\\$driver\\{$driver}{$availableVersion}QueryBuilder";
                if (class_exists($driverClass)) {
                    return $driverClass;
                }
            }
        }

        return null;
    }

    private static function normalizeVersion(array $versionParts): string
    {
        $normalized = array_map(function ($part) {
            return str_pad($part, 2, '0', STR_PAD_LEFT);
        }, $versionParts);

        return implode('', $normalized);
    }
}