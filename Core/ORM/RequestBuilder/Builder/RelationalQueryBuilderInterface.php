<?php

namespace Core\RequestBuilder\Builder;

interface RelationalQueryBuilderInterface
{
    //Gestion des données
    public function select(array $fields): self;
    public function from(string $table): self;
    public function where(array $conditions): self;
    public function orderBy(array $order_by): self;
    public function insert(string $table, array $data): self;
    public function update(string $table, array $data): self;
    public function delete(string $table): self;

    // Gestion des tables
    public function createTable(string $table, array $columns): self;
    public function dropTable(string $table): self;
    public function alterTable(string $table, array $modifications): self;
}