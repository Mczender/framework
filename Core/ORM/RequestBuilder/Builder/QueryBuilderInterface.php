<?php

namespace Core\RequestBuilder\Builder;

interface QueryBuilderInterface
{
    public function getQuery(): string;
}