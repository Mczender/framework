<?php

namespace Core\RequestBuilder\Builder;

interface ColumnQueryBuilderInterface
{
    public function select(string $table, array $columns, array $conditions = []): self;
    public function insert(string $table, array $data): self;
    public function update(string $table, array $conditions, array $data): self;
    public function delete(string $table, array $conditions): self;
}