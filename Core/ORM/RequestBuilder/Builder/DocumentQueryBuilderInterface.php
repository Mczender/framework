<?php

namespace Core\RequestBuilder\Builder;

interface DocumentQueryBuilderInterface
{
    public function find(array $filter, array $options = []): self;
    public function insertOne(string $collection, array $document): self;
    public function updateOne(string $collection, array $filter, array $update): self;
    public function deleteOne(string $collection, array $filter): self;
}