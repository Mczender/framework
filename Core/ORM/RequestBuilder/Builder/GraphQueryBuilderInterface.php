<?php

namespace Core\RequestBuilder\Builder;

interface GraphQueryBuilderInterface
{
    public function createNode(string $label, array $properties): self;
    public function createRelationship(string $fromNode, string $relationshipType, string $toNode, array $properties): self;
    public function findNode(string $label, array $conditions): self;
    public function findRelationship(string $fromNode, string $relationshipType, string $toNode): self;
}