<?php

namespace Core\RequestBuilder\Builder\Relational;

use Core\RequestBuilder\Builder\RelationalQueryBuilderInterface;
use Core\RequestBuilder\Query\MySQLQuery;
use Core\RequestBuilder\Query\QueryInterface;

abstract class AbstractRelationalQueryBuilder implements RelationalQueryBuilderInterface
{
    protected string $table;
    protected array $fields = [];
    protected array $conditions = [];
    protected array $data = [];
    protected string $query = '';
    protected array $params = [];
    protected array $functions = [];

    public function select(array $fields): self
    {
        $this->fields = array_map([$this, 'applyVersionSpecificFunctions'], $fields);
        return $this;
    }

    public function from(string $table): self
    {
        $this->table = $table;
        return $this;
    }

    public function where(array $conditions): self
    {
        $this->conditions = array_map([$this, 'applyVersionSpecificFunctions'], $conditions);
        return $this;
    }

    protected function setWhere(string $query): string
    {
        if (!empty($this->conditions)) {
            $query .= " WHERE " . implode(" AND ", array_map(
                    fn($key, $value) => "$key = :$key",
                    array_keys($this->conditions),
                    array_values($this->conditions)
                ));
            $this->params = array_merge($this->params, $this->conditions);
        }

        return $query;
    }

    public function insert(string $table, array $data): self
    {
        $this->table = $table;
        $this->data = $data;
        $columns = implode(", ", array_keys($data));
        $placeholders = implode(", ", array_map(fn($key) => ":$key", array_keys($data)));
        $this->query = "INSERT INTO {$this->table} ({$columns}) VALUES ({$placeholders})";
        $this->params = $data;

        return $this;
    }

    public function update(string $table, array $data): self
    {
        $this->table = $table;
        $this->data = $data;
        $set = implode(", ", array_map(fn($key) => "$key = :$key", array_keys($data)));
        $this->query = "UPDATE {$this->table} SET {$set}";
        $this->params = $data;
        $this->query = $this->setWhere($this->query);

        return $this;
    }

    public function delete(string $table): self
    {
        $this->table = $table;
        $this->query = "DELETE FROM {$this->table}";
        $this->query = $this->setWhere($this->query);

        return $this;
    }

    public function orderBy(array $order_by): self
    {
        $this->query .= " ORDER BY " . implode(", ", array_map(fn($key, $value) => "$key $value", array_keys($order_by), array_values($order_by)));

        return $this;
    }

    public function createTable(string $table, array $columns): self
    {
        $columnsDef = implode(", ", array_map(fn($name, $definition) => "$name $definition", array_keys($columns), $columns));
        $this->query = "CREATE TABLE $table ($columnsDef)";

        return $this;
    }

    public function dropTable(string $table): self
    {
        $this->query = "DROP TABLE $table";

        return $this;
    }

    public function alterTable(string $table, array $modifications): self
    {
        $alterations = implode(", ", $modifications);
        $this->query = "ALTER TABLE $table $alterations";

        return $this;
    }

    public function getQuery(): QueryInterface
    {
        if (empty($this->query)) {
            if (!empty($this->fields)) {
                $fields = implode(", ", $this->fields);
                $this->query = "SELECT {$fields} FROM {$this->table}";
                $this->query = $this->setWhere($this->query);
            }
        }

        return new MySQLQuery($this->query, $this->params);
    }

    public function registerFunction(string $name, callable $function): void
    {
        $this->functions[$name] = $function;
    }

    protected function applyVersionSpecificFunctions(array|string $field): array|string
    {
        if (is_array($field) && isset($field['function'])) {
            $functionName = $field['function'];
            if (isset($this->functions[$functionName])) {
                return $this->functions[$functionName](...$field['args']);
            }
        }

        return $field;
    }
}