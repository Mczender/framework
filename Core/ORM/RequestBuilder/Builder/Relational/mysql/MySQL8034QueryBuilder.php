<?php

namespace Core\RequestBuilder\Builder\Relational\mysql;

use Core\RequestBuilder\Builder\Relational\AbstractRelationalQueryBuilder;
use Core\RequestBuilder\Builder\Relational\GlobalSqlFunctionsTrait;

class MySQL8034QueryBuilder extends AbstractRelationalQueryBuilder
{
    use GlobalSqlFunctionsTrait;

    public function __construct()
    {
        $this->registerGlobalFunctions();
    }
}