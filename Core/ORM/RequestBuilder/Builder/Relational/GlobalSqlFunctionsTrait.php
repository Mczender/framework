<?php

namespace Core\RequestBuilder\Builder\Relational;

/**
 * Trait GlobalSqlFunctionsTrait à inclure uniquement dans les drivers SQL
 * @package Core\RequestBuilder\Builder\Relational
 */
trait GlobalSqlFunctionsTrait
{
    public function registerGlobalFunctions(): void
    {
        // Fonctions d’agrégation
        $this->registerFunction('AVG', [$this, 'avg']);
        $this->registerFunction('COUNT', [$this, 'count']);
        $this->registerFunction('MAX', [$this, 'max']);
        $this->registerFunction('MIN', [$this, 'min']);
        $this->registerFunction('SUM', [$this, 'sum']);

        // Fonctions de chaînes de caractères
        $this->registerFunction('CONCAT', [$this, 'concat']);
        $this->registerFunction('LENGTH', [$this, 'length']);
        $this->registerFunction('REPLACE', [$this, 'replace']);
        $this->registerFunction('SOUNDEX', [$this, 'soundex']);
        $this->registerFunction('SUBSTRING', [$this, 'substring']);
        $this->registerFunction('LEFT', [$this, 'left']);
        $this->registerFunction('RIGHT', [$this, 'right']);
        $this->registerFunction('REVERSE', [$this, 'reverse']);
        $this->registerFunction('TRIM', [$this, 'trim']);
        $this->registerFunction('LTRIM', [$this, 'ltrim']);
        $this->registerFunction('RTRIM', [$this, 'rtrim']);
        $this->registerFunction('LPAD', [$this, 'lpad']);
        $this->registerFunction('UPPER', [$this, 'upper']);
        $this->registerFunction('LOWER', [$this, 'lower']);
        $this->registerFunction('UCASE', [$this, 'ucase']);
        $this->registerFunction('LCASE', [$this, 'lcase']);
        $this->registerFunction('LOCATE', [$this, 'locate']);
        $this->registerFunction('INSTR', [$this, 'instr']);

        // Fonctions mathématiques / numérique
        $this->registerFunction('RAND', [$this, 'rand']);
        $this->registerFunction('ROUND', [$this, 'round']);

        // Fonctions de dates et d’heures
        $this->registerFunction('DATE_FORMAT', [$this, 'dateFormat']);
        $this->registerFunction('DATEDIFF', [$this, 'dateDiff']);
        $this->registerFunction('DAYOFWEEK', [$this, 'dayOfWeek']);
        $this->registerFunction('MONTH', [$this, 'month']);
        $this->registerFunction('NOW', [$this, 'now']);
        $this->registerFunction('SEC_TO_TIME', [$this, 'secToTime']);
        $this->registerFunction('TIMEDIFF', [$this, 'timeDiff']);
        $this->registerFunction('TIMESTAMP', [$this, 'timestamp']);
        $this->registerFunction('YEAR', [$this, 'year']);
    }

    // Fonctions d’agrégation
    public function avg(string $field): string
    {
        return "AVG($field)";
    }

    public function count(string $field): string
    {
        return "COUNT($field)";
    }

    public function max(string $field): string
    {
        return "MAX($field)";
    }

    public function min(string $field): string
    {
        return "MIN($field)";
    }

    public function sum(string $field): string
    {
        return "SUM($field)";
    }

    // Fonctions de chaînes de caractères
    public function concat(string ...$fields): string
    {
        return 'CONCAT(' . implode(', ', $fields) . ')';
    }

    public function length(string $field): string
    {
        return "LENGTH($field)";
    }

    public function replace(string $field, string $search, string $replace): string
    {
        return "REPLACE($field, '$search', '$replace')";
    }

    public function soundex(string $field): string
    {
        return "SOUNDEX($field)";
    }

    public function substring(string $field, int $start, int $length = null): string
    {
        return $length ? "SUBSTRING($field, $start, $length)" : "SUBSTRING($field, $start)";
    }

    public function left(string $field, int $number): string
    {
        return "LEFT($field, $number)";
    }

    public function right(string $field, int $number): string
    {
        return "RIGHT($field, $number)";
    }

    public function reverse(string $field): string
    {
        return "REVERSE($field)";
    }

    public function trim(string $field): string
    {
        return "TRIM($field)";
    }

    public function ltrim(string $field): string
    {
        return "LTRIM($field)";
    }

    public function rtrim(string $field): string
    {
        return "RTRIM($field)";
    }

    public function lpad(string $field, int $length, string $padString): string
    {
        return "LPAD($field, $length, '$padString')";
    }

    public function upper(string $field): string
    {
        return "UPPER($field)";
    }

    public function lower(string $field): string
    {
        return "LOWER($field)";
    }

    public function ucase(string $field): string
    {
        return "UCASE($field)";
    }

    public function lcase(string $field): string
    {
        return "LCASE($field)";
    }

    public function locate(string $substr, string $field, int $start = 1): string
    {
        return "LOCATE('$substr', $field, $start)";
    }

    public function instr(string $field, string $substr): string
    {
        return "INSTR($field, '$substr')";
    }

    // Fonctions mathématiques / numérique
    public function rand(): string
    {
        return "RAND()";
    }

    public function round(string $field, int $decimals = 0): string
    {
        return "ROUND($field, $decimals)";
    }

    // Fonctions de dates et d’heures
    public function dateFormat(string $field, string $format): string
    {
        return "DATE_FORMAT($field, '$format')";
    }

    public function dateDiff(string $date1, string $date2): string
    {
        return "DATEDIFF($date1, $date2)";
    }

    public function dayOfWeek(string $field): string
    {
        return "DAYOFWEEK($field)";
    }

    public function month(string $field): string
    {
        return "MONTH($field)";
    }

    public function now(): string
    {
        return "NOW()";
    }

    public function secToTime(string $seconds): string
    {
        return "SEC_TO_TIME($seconds)";
    }

    public function timeDiff(string $time1, string $time2): string
    {
        return "TIMEDIFF($time1, $time2)";
    }

    public function timestamp(string $field): string
    {
        return "TIMESTAMP($field)";
    }

    public function year(string $field): string
    {
        return "YEAR($field)";
    }
}