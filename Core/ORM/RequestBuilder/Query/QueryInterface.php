<?php

namespace Core\RequestBuilder\Query;

interface QueryInterface
{
    public function __construct(string $query, array $params = []);

    public function getConnexion(): mixed;
}