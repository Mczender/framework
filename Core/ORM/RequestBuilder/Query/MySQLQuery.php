<?php

namespace Core\RequestBuilder\Query;

use Core\Config\bdd\Connexion;
use Core\Config\Exceptions\NotBddConfigException;
use Core\Config\Exceptions\WrongYamlConfigException;
use Core\RequestBuilder\Query\QueryInterface;

class MySQLQuery implements QueryInterface
{
    private static ?\PDO $connexion = null;

    public function __construct(private readonly string $query, private readonly array $params = [])
    {
    }

    /**
     * @return mixed
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public function getConnexion(): mixed
    {
        if (self::$connexion === null) {
            [$host, $port] = explode(':', Connexion::getHost());
            self::$connexion = new \PDO('mysql:host=' . $host . ';port=' . $port . ';dbname=' . Connexion::getDatabase(), Connexion::getUser(), Connexion::getPassword());
        }

        return self::$connexion;
    }

    /**
     * @return array
     * @throws NotBddConfigException
     * @throws WrongYamlConfigException
     */
    public function fetch(): array
    {
        $stmt = $this->getConnexion()->prepare($this->query);
        $stmt->execute($this->params);

        return $stmt->fetch();
    }

    /**
     * @throws WrongYamlConfigException
     * @throws NotBddConfigException
     */
    public function fetchAll(): array
    {
        $stmt = $this->getConnexion()->prepare($this->query);
        $stmt->execute($this->params);

        return $stmt->fetchAll();
    }

    /**
     * @throws WrongYamlConfigException
     * @throws NotBddConfigException
     */
    public function execute(): bool
    {
        $stmt = $this->getConnexion()->prepare($this->query);

        return $stmt->execute($this->params);
    }
}