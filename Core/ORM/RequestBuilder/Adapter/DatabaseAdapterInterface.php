<?php

namespace Core\RequestBuilder\Adapter;

use Core\RequestBuilder\Builder\QueryBuilderInterface;

interface DatabaseAdapterInterface
{
    public function __construct(QueryBuilderInterface $queryBuilder);

    public function find(string $table, array $fields, array $conditions = [], array $order_by = []): array;
    public function insert(string $table, array $data): void;
    public function update(string $table, array $filter, array $data): void;
    public function delete(string $table, array $filter): void;

    public function createStructure(string $table, array $columns): void;
    public function updateStructure(string $table, array $modifications): void;
    public function deleteStructure(string $table): void;
}