<?php

namespace Core\RequestBuilder\Adapter;

use Core\RequestBuilder\Adapter\DatabaseAdapterInterface;
use Core\RequestBuilder\Builder\QueryBuilderInterface;
use Core\RequestBuilder\Builder\RelationalQueryBuilderInterface;

readonly class RelationalAdapter implements DatabaseAdapterInterface
{

    public function __construct(private QueryBuilderInterface|RelationalQueryBuilderInterface $queryBuilder)
    {
    }

    public function find(string $table, array $fields, array $conditions = [], array $order_by = []): array
    {
        $this->queryBuilder
            ->select($fields)
            ->from($table)
            ->where($conditions)
            ->orderBy($order_by)
            ->getQuery();
        ;
    }

    public function insert(string $table, array $data): void
    {
        // TODO: Implement insert() method.
    }

    public function update(string $table, array $filter, array $data): void
    {
        // TODO: Implement update() method.
    }

    public function delete(string $table, array $filter): void
    {
        // TODO: Implement delete() method.
    }

    public function createStructure(string $table, array $columns): void
    {
        // TODO: Implement createStructure() method.
    }

    public function updateStructure(string $table, array $modifications): void
    {
        // TODO: Implement updateStructure() method.
    }

    public function deleteStructure(string $table): void
    {
        // TODO: Implement deleteStructure() method.
    }
}