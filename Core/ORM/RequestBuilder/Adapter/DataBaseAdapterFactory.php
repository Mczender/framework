<?php

namespace Core\RequestBuilder\Adapter;

use Core\Config\bdd\Connexion;
use Core\Config\Exceptions\NotBddConfigException;
use Core\Config\Exceptions\WrongYamlConfigException;
use Core\ORM\RequestBuilder\Builder\QueryBuilderFactory;

class DataBaseAdapterFactory
{
    private const array SUPPORTED_DATABASE_DRIVERS = [
        'relational' => ['mysql', 'pgsql', 'sqlite'],
        'documentaire' => ['mongodb']
    ];

    /**
     * @throws WrongYamlConfigException
     * @throws NotBddConfigException
     */
    public static function create(): DatabaseAdapterInterface
    {
        $bddType = Connexion::getType();

        foreach (self::SUPPORTED_DATABASE_DRIVERS as $type => $drivers) {
            if (in_array($bddType, $drivers)) {
                $adapter = 'Core\\ORM\\RequestBuilder\\Adapter\\' . ucfirst($type) . 'Adapter';
                $queryBuilder = QueryBuilderFactory::create($type, $bddType);

                return new $adapter(new $queryBuilder());
            }
        }

        throw new WrongYamlConfigException("Le driver de base de données $bddType n'est pas supporté.", 500);
    }
}