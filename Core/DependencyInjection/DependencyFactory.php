<?php

namespace Core\DependencyInjection;

interface DependencyFactory
{
    public function create(): Object;
}