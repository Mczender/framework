<?php

namespace Core\DependencyInjection;

use Core\Utils\Collections\Collection;
use Core\Utils\Collections\Exceptions\BadMatchCollectionTypeException;

class DependencyContainer
{
    private Collection $dependencies;

    public function __construct()
    {
        $this->dependencies = new Collection(Collection::TYPE_STRING);
    }

    /**
     * @throws BadMatchCollectionTypeException
     */
    public function register(string $name, string $class): bool
    {
        if (!$this->dependencies->has($name)) {
            $this->dependencies->set($name, $class);
            return true;
        }

        return false;
    }
}