<?php

namespace Core\DependencyInjection;

use ReflectionException;

class ObjectFactory  
{
    public function __construct(private DependencyContainer $container)
    {
    }

    /**
     * @throws ReflectionException
     */
    public function getInstance(string $class): Object
    {
        $reflectionClass = new \ReflectionClass($class);
        $arguments = $reflectionClass->getConstructor()->getParameters();

        foreach ($arguments as $argument) {
            $argType = $argument->getType()->getName();
        }
    }

    private function createDependency(DependencyFactory $factory): Object
    {

    }
}