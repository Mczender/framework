<?php

use Core\Command\Runner\CommandRunner;

require_once 'Core/Helpers/PhpHelpers.php';
require_once 'Core/autoload.php';

if (isset($argv)) {
    $commandRunner = new CommandRunner();
    $commandRunner->runCommand($argv[1]);
}

